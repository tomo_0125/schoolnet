# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170221133402) do

  create_table "answer_choices", force: :cascade do |t|
    t.integer  "answer_history_id", limit: 4
    t.integer  "question_id",       limit: 4
    t.integer  "choice_id",         limit: 4
    t.text     "free_word",         limit: 65535
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "answer_choices", ["answer_history_id"], name: "index_answer_choices_on_answer_history_id", using: :btree
  add_index "answer_choices", ["choice_id"], name: "index_answer_choices_on_choice_id", using: :btree
  add_index "answer_choices", ["question_id"], name: "index_answer_choices_on_question_id", using: :btree

  create_table "answer_histories", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "content_id", limit: 4
    t.integer  "sequence",   limit: 4
    t.integer  "score",      limit: 4
    t.boolean  "pass_flag"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "answer_histories", ["content_id"], name: "index_answer_histories_on_content_id", using: :btree
  add_index "answer_histories", ["user_id"], name: "index_answer_histories_on_user_id", using: :btree

  create_table "application_categories", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "route",             limit: 255
    t.string   "icon_file_name",    limit: 255
    t.string   "icon_content_type", limit: 255
    t.integer  "icon_file_size",    limit: 4
    t.datetime "icon_updated_at"
    t.integer  "division",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "application_setting_applications", force: :cascade do |t|
    t.integer "application_setting_id", limit: 4
    t.integer "application_id",         limit: 4
  end

  add_index "application_setting_applications", ["application_id"], name: "index_application_setting_applications_on_application_id", using: :btree
  add_index "application_setting_applications", ["application_setting_id"], name: "index_application_setting_applications_on_application_setting_id", using: :btree

  create_table "application_settings", force: :cascade do |t|
    t.integer  "sort",              limit: 4
    t.integer  "user_id",           limit: 4
    t.string   "icon_file_name",    limit: 255
    t.string   "icon_content_type", limit: 255
    t.integer  "icon_file_size",    limit: 4
    t.datetime "icon_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "application_users", force: :cascade do |t|
    t.integer "application_id", limit: 4
    t.integer "user_id",        limit: 4
  end

  add_index "application_users", ["application_id"], name: "index_application_users_on_application_id", using: :btree
  add_index "application_users", ["user_id"], name: "index_application_users_on_user_id", using: :btree

  create_table "applications", force: :cascade do |t|
    t.integer  "application_category_id", limit: 4
    t.string   "name",                    limit: 255
    t.string   "route",                   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applications", ["application_category_id"], name: "index_applications_on_application_category_id", using: :btree

  create_table "board_comments", force: :cascade do |t|
    t.integer  "board_id",   limit: 4
    t.integer  "user_id",    limit: 4
    t.text     "comment",    limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "board_comments", ["board_id"], name: "index_board_comments_on_board_id", using: :btree
  add_index "board_comments", ["user_id"], name: "index_board_comments_on_user_id", using: :btree

  create_table "boards", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "title",      limit: 255
    t.text     "body",       limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "boards", ["user_id"], name: "index_boards_on_user_id", using: :btree

  create_table "choices", force: :cascade do |t|
    t.integer  "question_id", limit: 4
    t.text     "sentence",    limit: 65535
    t.boolean  "free_flag"
    t.boolean  "answer_flag"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "choices", ["question_id"], name: "index_choices_on_question_id", using: :btree

  create_table "communities", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "body",       limit: 65535
    t.boolean  "limited"
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.datetime "deleted_at"
  end

  add_index "communities", ["user_id"], name: "index_communities_on_user_id", using: :btree

  create_table "communities_users", force: :cascade do |t|
    t.integer  "community_id", limit: 4
    t.integer  "user_id",      limit: 4
    t.integer  "status",       limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "communities_users", ["community_id"], name: "index_communities_users_on_community_id", using: :btree
  add_index "communities_users", ["user_id"], name: "index_communities_users_on_user_id", using: :btree

  create_table "community_comments", force: :cascade do |t|
    t.integer  "community_id", limit: 4
    t.integer  "user_id",      limit: 4
    t.text     "comment",      limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "community_comments", ["community_id"], name: "index_community_comments_on_community_id", using: :btree
  add_index "community_comments", ["user_id"], name: "index_community_comments_on_user_id", using: :btree

  create_table "contents", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.string   "title",          limit: 255
    t.text     "body",           limit: 65535
    t.integer  "item_type",      limit: 4
    t.string   "file_extension", limit: 255
    t.string   "content_type",   limit: 255
    t.time     "play_time"
    t.integer  "clear_score",    limit: 4
    t.integer  "perfect_score",  limit: 4
    t.datetime "released_at"
    t.datetime "stopped_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.datetime "deleted_at"
  end

  add_index "contents", ["deleted_at"], name: "index_contents_on_deleted_at", using: :btree

  create_table "contents_media", force: :cascade do |t|
    t.integer  "content_id", limit: 4
    t.integer  "medium_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "contents_media", ["content_id"], name: "index_contents_media_on_content_id", using: :btree
  add_index "contents_media", ["medium_id"], name: "index_contents_media_on_medium_id", using: :btree

  create_table "contents_users", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "content_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "media", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "item_file_name",    limit: 255
    t.string   "item_content_type", limit: 255
    t.integer  "item_file_size",    limit: 4
    t.datetime "item_updated_at"
    t.text     "title",             limit: 65535
    t.integer  "encode_status",     limit: 4,     default: 2
  end

  add_index "media", ["user_id"], name: "index_media_on_user_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.integer  "content_id",    limit: 4
    t.string   "title",         limit: 255
    t.text     "body",          limit: 65535
    t.integer  "question_type", limit: 4
    t.integer  "test_score",    limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "questions", ["content_id"], name: "index_questions_on_content_id", using: :btree

  create_table "role_users", force: :cascade do |t|
    t.integer "role_id", limit: 4
    t.integer "user_id", limit: 4
  end

  add_index "role_users", ["role_id"], name: "index_role_users_on_role_id", using: :btree
  add_index "role_users", ["user_id"], name: "index_role_users_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               limit: 255,   default: "email", null: false
    t.string   "uid",                    limit: 255,   default: "",      null: false
    t.string   "encrypted_password",     limit: 255,   default: "",      null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "name",                   limit: 255
    t.string   "nickname",               limit: 255
    t.string   "image",                  limit: 255
    t.string   "email",                  limit: 255
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "school_id",              limit: 255,                     null: false
    t.string   "login_id",               limit: 255,                     null: false
  end

  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["login_id", "school_id"], name: "index_users_on_login_id_and_school_id", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  add_foreign_key "answer_choices", "answer_histories"
  add_foreign_key "answer_choices", "choices"
  add_foreign_key "answer_choices", "questions"
  add_foreign_key "answer_histories", "contents"
  add_foreign_key "answer_histories", "users"
  add_foreign_key "application_setting_applications", "application_settings"
  add_foreign_key "application_setting_applications", "applications"
  add_foreign_key "application_users", "applications"
  add_foreign_key "application_users", "users"
  add_foreign_key "board_comments", "boards"
  add_foreign_key "board_comments", "users"
  add_foreign_key "boards", "users"
  add_foreign_key "choices", "questions"
  add_foreign_key "communities", "users"
  add_foreign_key "communities_users", "communities"
  add_foreign_key "communities_users", "users"
  add_foreign_key "community_comments", "communities"
  add_foreign_key "community_comments", "users"
  add_foreign_key "contents_media", "contents"
  add_foreign_key "contents_media", "media"
  add_foreign_key "media", "users"
  add_foreign_key "questions", "contents"
  add_foreign_key "role_users", "roles"
  add_foreign_key "role_users", "users"
end
