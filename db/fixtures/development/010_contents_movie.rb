Content.seed(:id) do |s|
	s.id = 101
  s.user_id = 3
  s.title = "映像1"
  s.body = "映像1です"
  s.item_type = Content.item_types[:movie]
  s.file_extension = "mp4"
  s.play_time = nil
  s.clear_score = nil
  s.perfect_score = nil
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Content.seed(:id) do |s|
	s.id = 102
  s.user_id = 3
  s.title = "映像2"
  s.body = "映像2です"
  s.item_type = Content.item_types[:movie]
  s.file_extension = "mp4"
  s.play_time = nil
  s.clear_score = nil
  s.perfect_score = nil
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Content.seed(:id) do |s|
	s.id = 103
  s.user_id = 3
  s.title = "映像3"
  s.body = "映像3です"
  s.item_type = Content.item_types[:movie]
  s.file_extension = "mp4"
  s.play_time = nil
  s.clear_score = nil
  s.perfect_score = nil
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Medium.seed(:id) do |s|
  s.id = 101
  s.item = File.open("db/fixtures/development/movie.mp4")
	s.encode_status = Medium.encode_statuses[:wait]
end

Medium.seed(:id) do |s|
  s.id = 102
  s.item = File.open("db/fixtures/development/movie2.mp4")
	s.encode_status = Medium.encode_statuses[:wait]
end

Medium.seed(:id) do |s|
  s.id = 103
  s.item = File.open("db/fixtures/development/movie3.mp4")
	s.encode_status = Medium.encode_statuses[:processing]
end

Medium.seed(:id) do |s|
  s.id = 104
  s.item = File.open("db/fixtures/development/movie3.mp4")
	s.encode_status = Medium.encode_statuses[:complete]
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 101
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 102
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 103
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 101
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 102
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 103
end



