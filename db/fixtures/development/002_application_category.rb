ApplicationCategory.seed(:id) do |s|
  s.id = 1
  s.name = "コミュニケーション"
  s.route = "communications"
  s.icon = File.open("app/assets/images/application_category/communication.png")
  s.division = 0
end

ApplicationCategory.seed(:id) do |s|
  s.id = 2
  s.name = "スケジュール"
  s.route = "schedules"
  s.icon = File.open("app/assets/images/application_category/schedule.png")
  s.division = 1
end

ApplicationCategory.seed(:id) do |s|
  s.id = 3
  s.name = "アップロード"
  s.route = "uploads"
  s.icon = File.open("app/assets/images/application_category/upload.png")
  s.division = 0
end

ApplicationCategory.seed(:id) do |s|
  s.id = 4
  s.name = "スタディアイテム"
  s.route = "study_items"
  s.icon = File.open("app/assets/images/application_category/study_item.png")
  s.division = 0
end

ApplicationCategory.seed(:id) do |s|
  s.id = 5
  s.name = "設定"
  s.route = "settings"
  s.icon = File.open("app/assets/images/application_category/setting.png")
  s.division = 0
end

