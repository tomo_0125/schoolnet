Content.seed(:id) do |s|
	s.id = 1
  s.user_id = 3
  s.title = "文書1"
  s.body = "文書1です"
  s.item_type = Content.item_types[:document]
  s.file_extension = "pdf"
  s.play_time = nil
  s.clear_score = nil
  s.perfect_score = nil
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Content.seed(:id) do |s|
	s.id = 2
  s.user_id = 3
  s.title = "文書2"
  s.body = "文書2です"
  s.item_type = Content.item_types[:document]
  s.file_extension = "pdf"
  s.play_time = nil
  s.clear_score = nil
  s.perfect_score = nil
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Content.seed(:id) do |s|
	s.id = 3
  s.user_id = 3
  s.title = "文書3"
  s.body = "文書3です"
  s.item_type = Content.item_types[:document]
  s.file_extension = "pdf"
  s.play_time = nil
  s.clear_score = nil
  s.perfect_score = nil
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Content.seed(:id) do |s|
	s.id = 4
  s.user_id = 4
  s.title = "文書4"
  s.body = "文書4です"
  s.item_type = Content.item_types[:document]
  s.file_extension = "pdf"
  s.play_time = nil
  s.clear_score = nil
  s.perfect_score = nil
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Medium.seed(:id) do |s|
  s.id = 1
  s.item = File.open("db/fixtures/development/sample.pdf")
	s.encode_status = Medium.encode_statuses[:complete]
end

Medium.seed(:id) do |s|
  s.id = 2
  s.item = File.open("db/fixtures/development/sample.pdf")
	s.encode_status = Medium.encode_statuses[:complete]
end

Medium.seed(:id) do |s|
  s.id = 3
  s.item = File.open("db/fixtures/development/sample.pdf")
	s.encode_status = Medium.encode_statuses[:complete]
end

Medium.seed(:id) do |s|
  s.id = 4
  s.item = File.open("db/fixtures/development/sample.pdf")
	s.encode_status = Medium.encode_statuses[:complete]
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 1
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 2
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 3
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 4
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 1
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 2
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 3
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 4
end


