Application.seed(:id) do |s|
  s.id = 1
  s.application_category = ApplicationCategory.find_by(route: "communications")
  s.name = "コミュニティ"
  s.route = "communities"
end

Application.seed(:id) do |s|
  s.id = 2
  s.application_category = ApplicationCategory.find_by(route: "communications")
  s.name = "メッセージ"
  s.route = "messages"
end

Application.seed(:id) do |s|
  s.id = 3
  s.application_category = ApplicationCategory.find_by(route: "communications")
  s.name = "掲示板"
  s.route = "boards"
end

Application.seed(:id) do |s|
  s.id = 4
  s.application_category = ApplicationCategory.find_by(route: "communications")
  s.name = "動画ライブラリ"
  s.route = "movie_uploads"
end

Application.seed(:id) do |s|
  s.id = 5
  s.application_category = ApplicationCategory.find_by(route: "communications")
  s.name = "文書ライブラリ"
  s.route = "file_uploads"
end

Application.seed(:id) do |s|
  s.id = 6
  s.application_category = ApplicationCategory.find_by(route: "communications")
  s.name = "アンケート"
  s.route = "enquetes"
end

Application.seed(:id) do |s|
  s.id = 7
  s.application_category = ApplicationCategory.find_by(route: "communications")
  s.name = "欠席連絡"
  s.route = "absenses"
end

Application.seed(:id) do |s|
  s.id = 8
  s.application_category = ApplicationCategory.find_by(route: "uploads")
  s.name = "動画アップロード"
  s.route = "movie_uploads"
end

Application.seed(:id) do |s|
  s.id = 9
  s.application_category = ApplicationCategory.find_by(route: "uploads")
  s.name = "文書アップロード"
  s.route = "file_uploads"
end

Application.seed(:id) do |s|
  s.id = 10
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "WEBテスト"
  s.route = "web_tests"
end

Application.seed(:id) do |s|
  s.id = 11
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "WEBテストの作成"
  s.route = "web_tests"
end

Application.seed(:id) do |s|
  s.id = 12
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "アンケート"
  s.route = "enquetes"
end

Application.seed(:id) do |s|
  s.id = 13
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "学習時間の提出"
  s.route = "study_plans"
end

Application.seed(:id) do |s|
  s.id = 14
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "学習時間の確認"
  s.route = "study_plans"
end

Application.seed(:id) do |s|
  s.id = 15
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "設備予約"
  s.route = "facilities"
end

Application.seed(:id) do |s|
  s.id = 16
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "動画ライブラリ"
  s.route = "movie_uploads"
end

Application.seed(:id) do |s|
  s.id = 17
  s.application_category = ApplicationCategory.find_by(route: "study_items")
  s.name = "文書ライブラリ"
  s.route = "file_uploads"
end

Application.seed(:id) do |s|
  s.id = 18
  s.application_category = ApplicationCategory.find_by(route: "settings")
  s.name = "NGワード設定"
  s.route = "ng_words"
end

Application.seed(:id) do |s|
  s.id = 19
  s.application_category = ApplicationCategory.find_by(route: "settings")
  s.name = "コミュニティ設定"
  s.route = "communities"
end

Application.seed(:id) do |s|
  s.id = 20
  s.application_category = ApplicationCategory.find_by(route: "settings")
  s.name = "緊急連絡網"
  s.route = "emergencies"
end

Application.seed(:id) do |s|
  s.id = 21
  s.application_category = ApplicationCategory.find_by(route: "settings")
  s.name = "設備管理"
  s.route = "facilities"
end

Application.seed(:id) do |s|
  s.id = 22
  s.application_category = ApplicationCategory.find_by(route: "settings")
  s.name = "ユーザー設定"
  s.route = "user_settings"
end






