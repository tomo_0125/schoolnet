RoleUser.seed(:id) do |s|
  s.id = 1
  s.user = User.find_by(name: "student1")
  s.role = Role.find_by(name: "student")
end

RoleUser.seed(:id) do |s|
  s.id = 2
  s.user = User.find_by(name: "student2")
  s.role = Role.find_by(name: "student")
end

RoleUser.seed(:id) do |s|
  s.id = 3
  s.user = User.find_by(name: "teacher1")
  s.role = Role.find_by(name: "student")
end

RoleUser.seed(:id) do |s|
  s.id = 4
  s.user = User.find_by(name: "teacher1")
  s.role = Role.find_by(name: "teacher")
end

RoleUser.seed(:id) do |s|
  s.id = 5
  s.user = User.find_by(name: "teacher2")
  s.role = Role.find_by(name: "student")
end

RoleUser.seed(:id) do |s|
  s.id = 6
  s.user = User.find_by(name: "teacher2")
  s.role = Role.find_by(name: "teacher")
end

RoleUser.seed(:id) do |s|
  s.id = 7
  s.user = User.find_by(name: "parent1")
  s.role = Role.find_by(name: "parent")
end

RoleUser.seed(:id) do |s|
  s.id = 8
  s.user = User.find_by(name: "parent2")
  s.role = Role.find_by(name: "teacher")
end

RoleUser.seed(:id) do |s|
  s.id = 9
  s.user = User.find_by(name: "admin1")
  s.role = Role.find_by(name: "student")
end

RoleUser.seed(:id) do |s|
  s.id = 10
  s.user = User.find_by(name: "admin1")
  s.role = Role.find_by(name: "teacher")
end

RoleUser.seed(:id) do |s|
  s.id = 11
  s.user = User.find_by(name: "admin1")
  s.role = Role.find_by(name: "parent")
end

RoleUser.seed(:id) do |s|
  s.id = 11
  s.user = User.find_by(name: "admin1")
  s.role = Role.find_by(name: "admin")
end

