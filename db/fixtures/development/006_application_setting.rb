icons = []
icons[1] = 'applications/communication/community.png'
icons[2] = 'applications/communication/massage.png'
icons[3] = 'applications/communication/bulletin_board.png'
icons[4] = 'applications/communication/video_library.png'
icons[5] = 'applications/communication/file_library.png'
icons[6] = 'applications/communication/questionnaire.png'
icons[7] = 'applications/communication/absent.png'
icons[8] = 'applications/upload/video_upload.png'
icons[9] = 'applications/upload/file_upload.png'
icons[10] = 'applications/study_item/web_exam.png'
icons[11] = 'applications/study_item/make_web_exam.png'
icons[12] = 'applications/study_item/questionnaire.png'
icons[13] = 'applications/study_item/study_houres.png'
icons[14] = 'applications/study_item/study_houres_check.png'
icons[15] = 'applications/study_item/reservation_equipment.png'
icons[16] = 'applications/study_item/video_library.png'
icons[17] = 'applications/study_item/file_library.png'
icons[18] = 'applications/setting/ng_word.png'
icons[19] = 'applications/setting/community_setting.png'
icons[20] = 'applications/setting/urgent_announcement.png'
icons[21] = 'applications/setting/equipment_management.png'
icons[22] = 'applications/setting/user_setting.png'

index_id = 1
ApplicationSettingApplication.destroy_all
ApplicationSetting.destroy_all

Application.all.each do |a|
  ApplicationSetting.seed(:id) do |as|
    as.id = index_id
    as.sort = a.id
    as.user = User.find_by(name: "student1")
    as.icon = File.open("app/assets/images/#{icons[a.id]}")
		index_id = index_id + 1
  end
	ApplicationSettingApplication.seed(:id) do |s|
		s.application_setting = ApplicationSetting.last
		s.application = a
	end
end

Application.all.each do |a|
  ApplicationSetting.seed(:id) do |s|
    s.id = index_id
    s.sort = a.id
    s.user = User.find_by(name: "student2")
    s.icon = File.open("app/assets/images/#{icons[a.id]}")
		index_id = index_id + 1
  end
	ApplicationSettingApplication.seed(:id) do |s|
		s.application_setting = ApplicationSetting.last
		s.application = a
	end
end

Application.all.each do |a|
  ApplicationSetting.seed(:id) do |s|
    s.id = index_id
    s.sort = a.id
    s.user = User.find_by(name: "teacher1")
    s.icon = File.open("app/assets/images/#{icons[a.id]}")
		index_id = index_id + 1
  end
	ApplicationSettingApplication.seed(:id) do |s|
		s.application_setting = ApplicationSetting.last
		s.application = a
	end
end

Application.all.each do |a|
  ApplicationSetting.seed(:id) do |s|
    s.id = index_id
    s.sort = a.id
    s.user = User.find_by(name: "teacher2")
    s.icon = File.open("app/assets/images/#{icons[a.id]}")
		index_id = index_id + 1
  end
	ApplicationSettingApplication.seed(:id) do |s|
		s.application_setting = ApplicationSetting.last
		s.application = a
	end
end

Application.all.each do |a|
  ApplicationSetting.seed(:id) do |s|
    s.id = index_id
    s.sort = a.id
    s.user = User.find_by(name: "parent1")
    s.icon = File.open("app/assets/images/#{icons[a.id]}")
		index_id = index_id + 1
  end
	ApplicationSettingApplication.seed(:id) do |s|
		s.application_setting = ApplicationSetting.last
		s.application = a
	end
end

Application.all.each do |a|
  ApplicationSetting.seed(:id) do |s|
    s.id = index_id
    s.sort = a.id
    s.user = User.find_by(name: "parent2")
    s.icon = File.open("app/assets/images/#{icons[a.id]}")
		index_id = index_id + 1
  end
	ApplicationSettingApplication.seed(:id) do |s|
		s.application_setting = ApplicationSetting.last
		s.application = a
	end
end

Application.all.each do |a|
  ApplicationSetting.seed(:id) do |s|
    s.id = index_id
    s.sort = a.id
    s.user = User.find_by(name: "admin1")
    s.icon = File.open("app/assets/images/#{icons[a.id]}")
		index_id = index_id + 1
  end
	ApplicationSettingApplication.seed(:id) do |s|
		s.application_setting = ApplicationSetting.last
		s.application = a
	end
end
