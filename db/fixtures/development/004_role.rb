Role.seed(:id) do |s|
  s.id = 1
  s.name = "student"
end

Role.seed(:id) do |s|
  s.id = 2
  s.name = "teacher"
end

Role.seed(:id) do |s|
  s.id = 3
  s.name = "parent"
end

Role.seed(:id) do |s|
  s.id = 4
  s.name = "admin"
end

