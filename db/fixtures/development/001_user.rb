User.seed(:id) do |s|
  s.id = 1
  s.school_id = "school1"
  s.email = "student1@m-and.co"
  s.name = "student1"
  s.login_id = "student1"
  s.password = "1234abcd"
  s.password_confirmation = "1234abcd"
end

User.seed(:id) do |s|
  s.id = 2
  s.school_id = "school1"
  s.email = "student2@m-and.co"
  s.name = "student2"
  s.login_id = "student2"
  s.password = "1234abcd"
  s.password_confirmation = "1234abcd"
end

User.seed(:id) do |s|
  s.id = 3
  s.school_id = "school1"
  s.email = "teacher1@m-and.co"
  s.name = "teacher1"
  s.login_id = "teacher1"
  s.password = "1234abcd"
  s.password_confirmation = "1234abcd"
end

User.seed(:id) do |s|
  s.id = 4
  s.school_id = "school1"
  s.email = "teacher2@m-and.co"
  s.name = "teacher2"
  s.login_id = "teacher2"
  s.password = "1234abcd"
  s.password_confirmation = "1234abcd"
end

User.seed(:id) do |s|
  s.id = 5
  s.school_id = "school1"
  s.email = "parent1@m-and.co"
  s.name = "parent1"
  s.login_id = "parent1"
  s.password = "1234abcd"
  s.password_confirmation = "1234abcd"
end

User.seed(:id) do |s|
  s.id = 6
  s.school_id = "school1"
  s.email = "parent2@m-and.co"
  s.name = "parent2"
  s.login_id = "parent2"
  s.password = "1234abcd"
  s.password_confirmation = "1234abcd"
end

User.seed(:id) do |s|
  s.id = 7
  s.school_id = "school1"
  s.email = "admin1@m-and.co"
  s.name = "admin1"
  s.login_id = "admin1"
  s.password = "1234abcd"
  s.password_confirmation = "1234abcd"
end

