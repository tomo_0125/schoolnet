Application.all.each do |a|
  ApplicationUser.seed(:application_id, :user_id) do |s|
    s.user_id = User.find_by(name: "student1").id
    s.application_id = a.id
  end
end

Application.all.each do |a|
  ApplicationUser.seed(:application_id, :user_id) do |s|
    s.user_id = User.find_by(name: "student2").id
    s.application_id = a.id
  end
end

Application.all.each do |a|
  ApplicationUser.seed(:application_id, :user_id) do |s|
    s.user_id = User.find_by(name: "teacher1").id
    s.application_id = a.id
  end
end

Application.all.each do |a|
  ApplicationUser.seed(:application_id, :user_id) do |s|
    s.user_id = User.find_by(name: "teacher2").id
    s.application_id = a.id
  end
end

Application.all.each do |a|
  ApplicationUser.seed(:application_id, :user_id) do |s|
    s.user_id = User.find_by(name: "parent1").id
    s.application_id = a.id
  end
end

Application.all.each do |a|
  ApplicationUser.seed(:application_id, :user_id) do |s|
    s.user_id = User.find_by(name: "parent2").id
    s.application_id = a.id
  end
end

Application.all.each do |a|
  ApplicationUser.seed(:application_id, :user_id) do |s|
    s.user_id = User.find_by(name: "admin1").id
    s.application_id = a.id
  end
end

