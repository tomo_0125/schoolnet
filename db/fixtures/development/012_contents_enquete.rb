Content.seed(:id) do |s|
	s.id = 401
  s.user_id = 3
  s.title = "アンケート1"
  s.body = "アンケート1です"
  s.item_type = Content.item_types[:enquete]
  s.file_extension = ""
  s.play_time = nil
  s.clear_score = 60
  s.perfect_score = 100
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Question.seed(:id) do |s|
	s.id = 411
  s.content_id = 401
  s.title = "1問目"
  s.body = "睡眠時間は平均どのくらいですか？"
  s.question_type = Question.question_types[:select_one]
  s.test_score = nil
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 411
  s.question_id = 411
  s.sentence = "3時間未満"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 412
  s.question_id = 411
  s.sentence = "3時間以上6時間未満"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 413
  s.question_id = 411
  s.sentence = "6時間以上8時間未満"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 414
  s.question_id = 411
  s.sentence = "8時間以上"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Question.seed(:id) do |s|
	s.id = 412
  s.content_id = 401
  s.title = "2問目"
  s.body = "普段眠気覚ましに何をしていますか？"
  s.question_type = Question.question_types[:multi_select]
  s.test_score = nil
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 421
  s.question_id = 412
  s.sentence = "コーヒーを飲む"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 422
  s.question_id = 412
  s.sentence = "体を動かす"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 423
  s.question_id = 412
  s.sentence = "換気をする"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 424
  s.question_id = 412
  s.sentence = "その他"
  s.free_flag = true
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Content.seed(:id) do |s|
	s.id = 402
  s.user_id = 3
  s.title = "アンケート2"
  s.body = "アンケート2です"
  s.item_type = Content.item_types[:test]
  s.file_extension = ""
  s.play_time = nil
  s.clear_score = 60
  s.perfect_score = 100
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Question.seed(:id) do |s|
	s.id = 421
  s.content_id = 402
  s.title = "1問目"
  s.body = "好きな色は何ですか？"
  s.question_type = Question.question_types[:select_one]
  s.test_score = nil
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 451
  s.question_id = 421
  s.sentence = "赤"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 452
  s.question_id = 421
  s.sentence = "青"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 453
  s.question_id = 421
  s.sentence = "緑"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 454
  s.question_id = 421
  s.sentence = "黄色"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 455
  s.question_id = 421
  s.sentence = "その他"
  s.free_flag = true
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 401
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 402
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 401
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 402
end


