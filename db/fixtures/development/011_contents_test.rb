Content.seed(:id) do |s|
	s.id = 301
  s.user_id = 3
  s.title = "テスト1"
  s.body = "テスト1です"
  s.item_type = Content.item_types[:test]
  s.file_extension = ""
  s.play_time = nil
  s.clear_score = 60
  s.perfect_score = 100
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Question.seed(:id) do |s|
	s.id = 111
  s.content_id = 301
  s.title = "第1問"
  s.body = "カナダの首都はオワタである？"
  s.question_type = Question.question_types[:select_one]
  s.test_score = 20
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 111
  s.question_id = 111
  s.sentence = "○"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 112
  s.question_id = 111
  s.sentence = "×"
  s.free_flag = false
  s.answer_flag = true
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Question.seed(:id) do |s|
	s.id = 112
  s.content_id = 301
  s.title = "第2問"
  s.body = "次の交通標識のうち\n人間の姿が描かれていないのはどれ？"
  s.question_type = Question.question_types[:select_one]
  s.test_score = 30
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 121
  s.question_id = 112
  s.sentence = "自転車専用"
  s.free_flag = false
  s.answer_flag = true
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 122
  s.question_id = 112
  s.sentence = "並進可"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 123
  s.question_id = 112
  s.sentence = "歩行者専用"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 124
  s.question_id = 112
  s.sentence = "横断歩道"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Question.seed(:id) do |s|
	s.id = 113
  s.content_id = 301
  s.title = "第3問"
  s.body = "次のうち、日清のカップ焼きそば\n「U.F.O.」のアルファベット３文字が意味するものを\n全て選びなさい"
  s.question_type = Question.question_types[:multi_select]
  s.test_score = 50
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 131
  s.question_id = 113
  s.sentence = "うまい"
  s.free_flag = false
  s.answer_flag = true
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 132
  s.question_id = 113
  s.sentence = "太い"
  s.free_flag = false
  s.answer_flag = true
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 133
  s.question_id = 113
  s.sentence = "大きい"
  s.free_flag = false
  s.answer_flag = true
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 134
  s.question_id = 113
  s.sentence = "おいしい"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Content.seed(:id) do |s|
	s.id = 302
  s.user_id = 3
  s.title = "テスト2"
  s.body = "テスト2です"
  s.item_type = Content.item_types[:test]
  s.file_extension = ""
  s.play_time = nil
  s.clear_score = 60
  s.perfect_score = 100
  s.released_at = DateTime.new(2017, 1, 1, 0, 0, 0)
  s.stopped_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.deleted_at = nil
end

Question.seed(:id) do |s|
	s.id = 211
  s.content_id = 302
  s.title = "第1問"
  s.body = "砂鉄も錆びる？"
  s.question_type = Question.question_types[:select_one]
  s.test_score = 50
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 211
  s.question_id = 211
  s.sentence = "○"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 212
  s.question_id = 211
  s.sentence = "×"
  s.free_flag = false
  s.answer_flag = true
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Question.seed(:id) do |s|
	s.id = 212
  s.content_id = 302
  s.title = "第2問"
  s.body = "「13日の金曜日」がある月の\n1日は何曜日？"
  s.question_type = Question.question_types[:select_one]
  s.test_score = 50
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 221
  s.question_id = 212
  s.sentence = "月曜日"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 222
  s.question_id = 212
  s.sentence = "水曜日"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 223
  s.question_id = 212
  s.sentence = "金曜日"
  s.free_flag = false
  s.answer_flag = false
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end

Choice.seed(:id) do |s|
  s.id = 224
  s.question_id = 212
  s.sentence = "日曜日"
  s.free_flag = false
  s.answer_flag = true
  s.created_at = DateTime.new(2020, 1, 1, 0, 0, 0)
  s.updated_at = DateTime.new(2020, 1, 1, 0, 0, 0)
end





ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 301
end

ContentsUser.seed(:id) do |s|
  s.user_id = 1
  s.content_id = 302
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 301
end

ContentsUser.seed(:id) do |s|
  s.user_id = 2
  s.content_id = 302
end


