class CreateCommunitiesUsers < ActiveRecord::Migration
  def change
    create_table :communities_users do |t|
      t.references :community, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :status

      t.timestamps null: false
    end
  end
end
