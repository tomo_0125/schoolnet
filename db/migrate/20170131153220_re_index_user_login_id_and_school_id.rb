class ReIndexUserLoginIdAndSchoolId < ActiveRecord::Migration
  def change
		remove_index :users, :email
		add_index :users, [:login_id, :school_id], unique: true
  end
end
