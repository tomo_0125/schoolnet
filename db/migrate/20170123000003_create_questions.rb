class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :content, index: true, foreign_key: true
      t.string :title
      t.text :body
      t.integer :question_type
      t.integer :test_score

      t.timestamps null: false
    end
  end
end
