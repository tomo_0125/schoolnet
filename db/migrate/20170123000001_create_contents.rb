class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.references :user
      t.string :title
      t.text :body
      t.integer :item_type
      t.string :file_extension
      t.string :content_type
      t.time :play_time
      t.integer :clear_score
      t.integer :perfect_score
      t.timestamp :released_at
      t.timestamp :stopped_at

      t.timestamps null: false
    end
  end
end
