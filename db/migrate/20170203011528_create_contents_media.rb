class CreateContentsMedia < ActiveRecord::Migration
  def change
    create_table :contents_media do |t|
      t.references :content, index: true, foreign_key: true
      t.references :medium, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
