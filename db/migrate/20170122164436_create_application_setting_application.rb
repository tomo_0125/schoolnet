class CreateApplicationSettingApplication < ActiveRecord::Migration
  def change
    create_table :application_setting_applications do |t|
      t.references :application_setting, index: true, foreign_key: true
      t.references :application, index: true, foreign_key: true
    end
  end
end
