class CreateContentsUsers < ActiveRecord::Migration
  def change
    create_table :contents_users do |t|
      t.references :user
      t.references :content

      t.timestamps null: false
    end
  end
end
