class CreateAnswerChoices < ActiveRecord::Migration
  def change
    create_table :answer_choices do |t|
      t.references :answer_history, index: true, foreign_key: true
      t.references :question, index: true, foreign_key: true
      t.references :choice, index: true, foreign_key: true
      t.text :free_word

      t.timestamps null: false
    end
  end
end
