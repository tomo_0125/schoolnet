class CreateApplicationSetting < ActiveRecord::Migration
  def change
    create_table :application_settings do |t|
      t.integer :sort
      t.references :user
      t.attachment :icon

      t.timestamps
    end
  end
end
