class CreateApplicationUser < ActiveRecord::Migration
  def change
    create_table :application_users do |t|
      t.references :application, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
    end
  end
end
