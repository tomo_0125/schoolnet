class CreateChoices < ActiveRecord::Migration
  def change
    create_table :choices do |t|
      t.references :question, index: true, foreign_key: true
      t.text :sentence
      t.boolean :free_flag
      t.boolean :answer_flag

      t.timestamps null: false
    end
  end
end
