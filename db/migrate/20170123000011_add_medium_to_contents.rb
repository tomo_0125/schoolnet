class AddMediumToContents < ActiveRecord::Migration
  def change
    add_reference :contents, :medium, index: true, foreign_key: true
  end
end
