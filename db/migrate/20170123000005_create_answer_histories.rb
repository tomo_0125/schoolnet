class CreateAnswerHistories < ActiveRecord::Migration
  def change
    create_table :answer_histories do |t|
      t.references :user, index: true, foreign_key: true
      t.references :content, index: true, foreign_key: true
      t.integer :sequence
      t.integer :score
      t.boolean :pass_flag

      t.timestamps null: false
    end
  end
end
