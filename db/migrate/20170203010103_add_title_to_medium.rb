class AddTitleToMedium < ActiveRecord::Migration
  def change
    add_column :media, :title, :text
  end
end
