class CreateApplicationCategory < ActiveRecord::Migration
  def change
    create_table :application_categories do |t|
      t.string :name
      t.string :route
      t.attachment :icon
      t.integer :division

      t.timestamps
    end
  end
end
