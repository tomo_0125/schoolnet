class RemoveMediumFromContent < ActiveRecord::Migration
  def change
    remove_reference :contents, :medium, index: true, foreign_key: true
  end
end
