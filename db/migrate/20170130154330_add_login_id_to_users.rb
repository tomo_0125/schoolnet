class AddLoginIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :school_id, :string, null: false
    add_column :users, :login_id, :string, null: false
  end
end
