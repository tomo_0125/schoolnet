class CreateCommunities < ActiveRecord::Migration
  def change
    create_table :communities do |t|
      t.string :title
      t.text :body
      t.boolean :limited
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
