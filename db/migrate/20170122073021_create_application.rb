class CreateApplication < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.references :application_category, index: true
      t.string :name
      t.string :route

      t.timestamps
    end
  end
end
