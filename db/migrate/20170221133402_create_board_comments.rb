class CreateBoardComments < ActiveRecord::Migration
  def change
    create_table :board_comments do |t|
      t.references :board, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.text :comment
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
