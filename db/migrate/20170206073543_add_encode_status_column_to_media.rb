class AddEncodeStatusColumnToMedia < ActiveRecord::Migration
  def change
		#enum encode_status: { wait: 0, processing: 1, complete: 2, error: 3 }
    add_column :media, :encode_status, :integer, default: 2
  end
end
