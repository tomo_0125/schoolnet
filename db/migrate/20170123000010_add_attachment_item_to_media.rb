class AddAttachmentItemToMedia < ActiveRecord::Migration
  def self.up
    change_table :media do |t|
      t.attachment :item
    end
  end

  def self.down
    remove_attachment :media, :item
  end
end
