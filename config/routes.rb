Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth', only: [:sign_in, :sign_out, :session],
		controllers: {
			sessions: 'sessions' # full module nesting
		}

  root to: "top#index"

	resources :absenses, only: [:index]
	resources :boards, only: [:index]
	resources :communities, only: [:index]
	resources :emergencies, only: [:index]
	resources :enquetes, only: [:index]
	resources :facilities, only: [:index]
	resources :file_uploads, only: [:index]
	resources :messages, only: [:index]
	resources :movie_uploads, only: [:index]
	resources :ng_words, only: [:index]
	resources :study_plans, only: [:index]
	resources :user_settings, only: [:index]
	resources :web_tests, only: [:index]
  resources :file_uploads, only: [:index]
  resources :movie_uploads, only: [:index]

  namespace :api, constraints: { format: 'json' } do
		resources :absenses
		resources :boards do
      resources :board_comments, only: [:create, :destroy]
    end
		resources :communities do
      resources :community_comments, only: [:create, :destroy]
      collection do
        get :users_list
      end
      member do
        put :join_community
      end
    end
		resources :emergencies
		resources :facilities
		resources :messages
		resources :ng_words
		resources :study_plans
		resources :user_settings

    resources :file_uploads do
      collection do
        get :users_list
      end
    end
    resources :movie_uploads do
      collection do
        get :users_list
      end
    end
    resources :web_tests do
      collection do
        get :users_list
        post :answer
      end
      member do
        get :users_histories
      end
    end
    resources :enquetes do
      collection do
        get :users_list
        post :answer
      end
    end
    resources :user_list, only: [:index]

    get 'web_tests/aggregate/:content_id(/:class_id)', to: 'web_tests#aggregate'
    get 'web_tests/high_score/:content_id(/:class_id)', to: 'web_tests#high_score'
    get 'enquetes/aggregate/:content_id(/:class_id)', to: 'enquetes#aggregate'
  end
end
