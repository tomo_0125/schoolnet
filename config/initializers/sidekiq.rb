url = ""
host = ""
if Rails.env.production?
	url = "localhost:6379"
	host = "localhost"
elsif Rails.env.staging?
	url = "localhost:6379"
	host = "localhost"
elsif Rails.env.development?
	url = "localhost:6379"
	host = "localhost"
elsif Rails.env.test?
	url = "localhost:6379"
	host = "localhost"
end

Sidekiq.configure_server do |config|
  config.redis = { url: "redis://#{url}", host: host, namespace: 'sidekiq' }
end
Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{url}", host: host, namespace: 'sidekiq' }
end

