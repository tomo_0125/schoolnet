FactoryGirl.define do
  factory :role_user1, class: RoleUser do
    role { FactoryGirl.create :role1 }
    user { FactoryGirl.create :user1 }
  end
end

