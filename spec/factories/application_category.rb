FactoryGirl.define do
  factory :application_category1, class: ApplicationCategory do
    name "コミュニケーション"
    route "communications"
    icon { File.open("spec/files/communication.png") }
    division 0
  end
end

