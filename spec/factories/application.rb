FactoryGirl.define do
  factory :application1, class: Application do
    application_category { FactoryGirl.create :application_category1 }
    name "コミュニティ"
    route "communications"
  end
end

