FactoryGirl.define do
  factory :user1, class: User do
    email "student1@m-and.co"
    name "student1"
    password "1234abcd"
    password_confirmation "1234abcd"
  end
end

