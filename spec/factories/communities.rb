FactoryGirl.define do
  factory :community do
    title "MyString"
    body "MyText"
    limited false
    user nil
  end
end
