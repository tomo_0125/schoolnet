FactoryGirl.define do
  factory :board_comment do
    board nil
    user nil
    comment "MyText"
    deleted_at "2017-02-21 22:34:04"
  end
end
