FactoryGirl.define do
  factory :board do
    user nil
    title "MyString"
    body "MyText"
    deleted_at "2017-02-21 22:13:45"
  end
end
