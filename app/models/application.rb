class Application < ActiveRecord::Base
  belongs_to :application_category

  has_many :application_setting_applications, :dependent => :destroy
  has_many :application_settings, :through => :application_setting_applications

  has_many :application_users, :dependent => :destroy
  has_many :users, :through => :application_users

	def to_response current_user
		{
			application: self,
			application_setting: application_settings.where(user_id: current_user.id).last.to_response,
		}
	end
end

