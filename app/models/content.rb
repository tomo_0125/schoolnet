class Content < ActiveRecord::Base
  acts_as_paranoid

  has_many :questions
  has_many :answer_histories
  has_many :contents_media, dependent: :destroy
  has_many :media, through: :contents_media
  has_many :contents_users, dependent: :destroy
  has_many :users, through: :contents_users
  belongs_to :user

  accepts_nested_attributes_for :media, allow_destroy: true
  accepts_nested_attributes_for :questions

  enum item_types: { document: 1, movie: 2, test: 3, enquete: 4 }

  scope :documents, -> { where(item_type: Content.item_types[:document]) }
  scope :movies,    -> { where(item_type: Content.item_types[:movie]) }
  scope :tests,     -> { where(item_type: Content.item_types[:test]) }
  scope :enquetes,  -> { where(item_type: Content.item_types[:enquete]) }
  scope :release_contents, -> { where('released_at <= ? AND (stopped_at IS NULL OR stopped_at >= ?)', DateTime.now, DateTime.now ) }
end
