class ContentsMedium < ActiveRecord::Base
  belongs_to :content
  belongs_to :medium
end
