class Community < ActiveRecord::Base
  acts_as_paranoid

  has_many :community_comments, dependent: :destroy
  has_many :communities_users, dependent: :destroy
  has_many :users, through: :communities_users
  belongs_to :user

  enum limiteds: { free: 0, limited: 1 }

  scope :viewable_communities, -> (user) { joins(:communities_users).where(communities_users: {user: user}).where(communities_users: {status: [CommunitiesUser.statuses[:ready], CommunitiesUser.statuses[:wait]]})}
  scope :ready_communities, -> (user) { joins(:communities_users).where(communities_users: {user: user}).where(communities_users: {status: CommunitiesUser.statuses[:ready]})}

  def join_status(user)
    return nil if user.blank?

    commu_user = CommunitiesUser.where(community: self).where(user: user).first
    if commu_user.present?
      commu_user.status
    else
      nil
    end
  end
end
