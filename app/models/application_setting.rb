class ApplicationSetting < ActiveRecord::Base
  belongs_to :user

  has_attached_file :icon, styles: { medium: "640x640>", thumb: "320x320>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :icon, content_type: /\Aimage\/.*\z/

	def to_response
		self.attributes.merge({
			icon_url: icon.url
		})
	end
end

