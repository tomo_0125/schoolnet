class AnswerChoice < ActiveRecord::Base
  belongs_to :answer_history
  belongs_to :question
  belongs_to :choice
end
