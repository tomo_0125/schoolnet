class Board < ActiveRecord::Base
  has_many :board_comments
  belongs_to :user
end
