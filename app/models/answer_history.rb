class AnswerHistory < ActiveRecord::Base
  has_many :answer_choices
  belongs_to :user
  belongs_to :content

  def web_test_results
    questions = []

    self.content.questions.each do |question|
      question_data = {
        question_id: question.id,
        judge: false,
        score: 0,
        choices: []
      }

      answer_choices = self.answer_choices.where(question_id: question.id)

      case question.question_type    
      when Question.question_types[:select_one]
        question_data = judge_select_one question, answer_choices
      when Question.question_types[:multi_select]
        question_data = judge_multi_select question, answer_choices
      end

      questions << question_data
    end

    questions
  end

private
  
  def judge_select_one question, answer_choices
    logger.debug "scoring_select_one question_id: #{question.id}"
  
    correct_flag = false
    score = 0
  
    # 正解の選択肢を取得
    correct_choice = question.choices.where(answer_flag: true).first
  
    # 正解の選択肢のIDと同じIDが入力されていれば正解
    answer_choices.each do |answer_choice|
      logger.debug "## choice_id: #{answer_choice.choice.id}"

      if correct_choice.id == answer_choice.choice.id
        logger.debug "## correct!"
        correct_flag = true 
        break
      end
    end
  
    if correct_flag
      score = question.test_score
    end
  
    {
      question_id: question.id,
      judge: correct_flag,
      score: score,
      choices: answer_choices
    }
  end
  
  def judge_multi_select question, answer_choices
    logger.debug "scoring_multi_select question_id: #{question.id}"
  
    correct_flag = true
    score = 0
    selected_choice_ids = []
  
    # 正解の選択肢を取得
    correct_choices = question.choices.where(answer_flag: true)
  
    # 不正解の選択肢を選択している場合は不正解
    answer_choices.each do |answer_choice|
      logger.debug "## answer_choice choice_id: #{answer_choice.choice.id} correct: #{answer_choice.choice.answer_flag}"
      unless answer_choice.choice.answer_flag
        logger.debug "## incorrect..."
        correct_flag = false
        break
      end
    end
  
    logger.debug "answer_choices count: #{correct_choices.count}  select_choices: #{answer_choices.count}"
  
    # 正解選択肢を選んでいない場合は不正解
    if correct_choices.count != answer_choices.count
      logger.debug "## incorrect..!"
      correct_flag = false
    end
  
    if correct_flag
      score = question.test_score
    end
  
    {
      question_id: question.id,
      judge: correct_flag,
      score: score,
      choices: answer_choices
    }
  end
end
