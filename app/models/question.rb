class Question < ActiveRecord::Base
  has_many :choices
  has_many :answer_choices
  belongs_to :content

  accepts_nested_attributes_for :choices

  enum question_types: { select_one: 1, multi_select: 2 }
end
