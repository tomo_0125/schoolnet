class ApplicationCategory < ActiveRecord::Base
  has_attached_file :icon, styles: { medium: "640x640>", thumb: "320x320>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :icon, content_type: /\Aimage\/.*\z/

	has_many :applications

	def to_response current_user
		{
			application_category: self.attributes.merge({
				icon_url: icon.url
			}),
			applications: applications.joins(:application_settings).where("application_settings.user_id = ?", current_user.id).map {|a| a.to_response(current_user)}
		}
	end
end
