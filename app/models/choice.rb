class Choice < ActiveRecord::Base
  has_many :answer_choices
  belongs_to :question
end
