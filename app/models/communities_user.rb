class CommunitiesUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :community

  enum statuses: { ready: 0, wait: 1, canceled: 2 }

  scope :ready_users, -> { where(status: CommunitiesUser.statuses[:ready]) }
  scope :limited_users, -> { where(status: CommunitiesUser.statuses[:limited]) }
end
