class Medium < ActiveRecord::Base
  has_many :contents_media, dependent: :destroy
  has_many :contents, through: :contents_media
  belongs_to :user

  has_attached_file :item, validate_media_type: false
  do_not_validate_attachment_file_type :item

  enum encode_status: { wait: 0, processing: 1, complete: 2, error: 3 }
end

