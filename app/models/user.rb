class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable,
          :rememberable, :trackable, :validatable,
					:authentication_keys => [:login_id, :school_id]
  include DeviseTokenAuth::Concerns::User

	validates_presence_of :login_id

	def email_required?
		false
	end

	def emial_changed?
		false
	end

  has_many :role_users, :dependent => :destroy
  has_many :roles, :through => :role_users
  has_many :contents_users, :dependent => :destroy
  has_many :contents, through: :contents_users
  has_many :communities_users, :dependent => :destroy
  has_many :communities, through: :communities_users
  has_many :answer_histories

  has_many :application_users, :dependent => :destroy
  has_many :applications, :through => :application_users

	def to_response
		{
			user: self,
			application_categories: ApplicationCategory.all.map {|a| a.to_response(self)}
		}
	end
end
