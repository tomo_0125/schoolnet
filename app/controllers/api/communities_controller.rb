class Api::CommunitiesController < AuthenticationController
  # -- community create
  # curl -XPOST -v -H "Access-Token: $ACCESS_TOKEN" -H "Client: $CLIENT" -H "Uid: $UID_MAIL" http://localhost:3000/api/communities \
  # -F 'user_ids[]=1' \
  # -F 'user_ids[]=2' \
  # -F 'community[title]=TITLE' \
  # -F 'community[body]=BODY' \
  # -F 'community[limited]=0' \
  #
  # limited  0: free(誰でも), 1: limited(承認制)

  # -- community update
  # curl -XPUT -v -H "Access-Token: $ACCESS_TOKEN" -H "Client: $CLIENT" -H "Uid: $UID_MAIL" http://localhost:3000/api/communities/10 \
  # -F 'user_ids[]=1' \
  # -F 'user_ids[]=2' \
  # -F 'user_ids[]=4' \
  # -F 'community[title]=TITLE' \
  # -F 'community[body]=BODY' \
  # -F 'community[limited]=1' \

  # -- community join
  # curl -XPUT -v -H "Access-Token: $ACCESS_TOKEN" -H "Client: $CLIENT" -H "Uid: $UID_MAIL" http://localhost:3000/api/communities/21/join_community

  before_action :set_community, only: [:show, :update, :destroy, :join_community]

  def index
    @communities = Community.where(user: current_user).page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  # ユーザが参加している・参加可能なコミュニティを取得
  def users_list
		@communities = Community.viewable_communities(current_user).page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  # コミュニティへの参加
  def join_community
    communities_user = CommunitiesUser.where(community_id: params[:id]).where(user_id: current_user.id).first

    unless communities_user.blank?
      communities_user.status = CommunitiesUser.statuses[:ready]
      communities_user.save
    end
    
    render formats: 'json', handlers: 'jbuilder'
  end

  def show
    render formats: 'json', handlers: 'jbuilder'
  end

  def create
    @community = Community.new(community_params)
    @community.user_id = current_user.id
    @community.users = set_users
    @community.communities_users.each do |commu_user|
      commu_user.status = get_status(commu_user)
    end
    @community.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def update
    communities_users = @community.communities_users

    Community.transaction do
      @community.update(community_params)
      @community.users = set_users
      @community.save!

      communities_users = CommunitiesUser.where(community: @community)
      communities_users.each do |commu_user|
        if commu_user.status.blank?
          commu_user.status = get_status(commu_user)
        end
        commu_user.save!
      end
    end

    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @community.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

private
  def set_community
    @community = Community.find(params[:id])
  end

  def community_params
    params
      .require(:community)
      .permit(
        :title,
        :body,
        :limited
    )
  end

  def set_users
    users = []
    return users if params[:user_ids].nil?

    # 自分自身を含める
    users << current_user

    params[:user_ids].each do |user_id|
      next if user_id == current_user.id
      user = User.find(user_id.to_i)
      users << user
    end
    users
  end

  def get_status commu_user
    # 自分自身は参加者とする
    return CommunitiesUser.statuses[:ready] if commu_user.user_id == current_user.id

    if community_params[:limited].present? && community_params[:limited].to_i == Community.limiteds[:limited]
      CommunitiesUser.statuses[:wait]
    else
      CommunitiesUser.statuses[:ready]
    end
  end
end
