class Api::CommunityCommentsController < AuthenticationController
  # -- community create
  # curl -XPOST -v -H "Access-Token: $ACCESS_TOKEN" -H "Client: $CLIENT" -H "Uid: $UID_MAIL" http://localhost:3000/api/communities/:community_id/community_comments \
  # -F 'community[comment]=COMMENT' \

  before_action :set_community_comment, only: [:destroy]

  def create
    @comment = CommunityComment.new(comment_params)
    @comment.community_id = params[:community_id]
    @comment.user_id = current_user.id
    @comment.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @comment.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

private
  def set_community_comment
    @comment = CommunityComment.find(params[:id])
  end

  def comment_params
    params
      .require(:community_comment)
      .permit(
        :comment
    )
  end
end
