class Api::FileUploadsController < AuthenticationController
  # usage
  #
  # -- index
  # curl -XGET -v http://localhost:3000/api/file_uploads
  # 
  # -- show
  # curl -XGET -v http://localhost:3000/api/file_uploads/4
  # 
  # -- create
  # curl -XPOST -v http://localhost:3000/api/file_uploads \
  # -F 'user_ids[]=1' \
  # -F 'user_ids[]=2' \
  # -F 'content[title]=TITLE' \
  # -F 'content[body]=BODY' \
  # -F 'content[released_at]=2017-02-04 20:00:00' \
  # -F 'content[stopped_at]=2018-02-10 00:00:00' \
  # -F 'content[media_attributes][0][id]=' \
  # -F 'content[media_attributes][0][title]=file1' \
  # -F 'content[media_attributes][0][item]=@/Users/shirakawatoshiaki/number/question_1.jpg' \
  # -F 'content[media_attributes][0][_destroy]=' \
  # -F 'content[media_attributes][1][id]=' \
  # -F 'content[media_attributes][1][title]=file2' \
  # -F 'content[media_attributes][1][item]=@/Users/shirakawatoshiaki/number/question_2.jpg' \
  # -F 'content[media_attributes][1][_destroy]=' \
  # -F 'content[media_attributes][2][id]=' \
  # -F 'content[media_attributes][2][title]=file3' \
  # -F 'content[media_attributes][2][item]=@/Users/shirakawatoshiaki/number/question_3.jpg' \
  # -F 'content[media_attributes][2][_destroy]=' \
  # 
  # -- update
  # curl -XPUT -v http://localhost:3000/api/file_uploads/1 \
  # -F 'user_ids[]=1' \
  # -F 'content[title]=TITLE!' \
  # -F 'content[body]=BODY!' \
  # -F 'content[released_at]=2017-02-04 20:00:00' \
  # -F 'content[stopped_at]=2018-02-10 00:00:00' \
  # -F 'content[media_attributes][0][id]=1' \
  # -F 'content[media_attributes][0][title]=file1!' \
  # -F 'content[media_attributes][0][item]=@/Users/shirakawatoshiaki/number/question_1d.jpg' \
  # -F 'content[media_attributes][0][_destroy]=' \
  # -F 'content[media_attributes][1][id]=2' \
  # -F 'content[media_attributes][1][title]=file2!' \
  # -F 'content[media_attributes][1][item]=@/Users/shirakawatoshiaki/number/question_2.jpg' \
  # -F 'content[media_attributes][1][_destroy]=' \
  # -F 'content[media_attributes][2][id]=3' \
  # -F 'content[media_attributes][2][title]=file3!' \
  # -F 'content[media_attributes][2][item]=@/Users/shirakawatoshiaki/number/question_3.jpg' \
  # -F 'content[media_attributes][2][_destroy]=' \
  # 
  # -- update(media remove)
  # curl -XPUT -v http://localhost:3000/api/file_uploads/1 \
  # -F 'user_ids[]=1' \
  # -F 'content[title]=TITLE!' \
  # -F 'content[body]=BODY!' \
  # -F 'content[released_at]=2017-02-04 20:00:00' \
  # -F 'content[stopped_at]=2018-02-10 00:00:00' \
  # -F 'content[media_attributes][0][id]=1' \
  # -F 'content[media_attributes][0][title]=file1!!' \
  # -F 'content[media_attributes][0][item]=@/Users/shirakawatoshiaki/number/question_1d.jpg' \
  # -F 'content[media_attributes][0][_destroy]=' \
  # -F 'content[media_attributes][1][id]=2' \
  # -F 'content[media_attributes][1][title]=file2!!' \
  # -F 'content[media_attributes][1][item]=@/Users/shirakawatoshiaki/number/question_2.jpg' \
  # -F 'content[media_attributes][1][_destroy]=' \
  # -F 'content[media_attributes][2][id]=3' \
  # -F 'content[media_attributes][2][title]=file3!!' \
  # -F 'content[media_attributes][2][item]=@/Users/shirakawatoshiaki/number/question_3.jpg' \
  # -F 'content[media_attributes][2][_destroy]=1' \
  # 
  # -- destroy
  # curl -XDELETE -v http://localhost:3000/api/file_uploads/1 \
  # -F 'content[title]=TITLE!' \
  # -F 'content[body]=BODY!' \
  # -F 'content[released_at]=2017-02-04 20:00:00' \
  # -F 'content[stopped_at]=2018-02-10 00:00:00' \
  # -F 'content[media_attributes][0][id]=1' \
  # -F 'content[media_attributes][0][title]=file1!!' \
  # -F 'content[media_attributes][0][item]=@/Users/shirakawatoshiaki/number/question_1d.jpg' \
  # -F 'content[media_attributes][0][_destroy]=' \
  # -F 'content[media_attributes][1][id]=2' \
  # -F 'content[media_attributes][1][title]=file2!!' \
  # -F 'content[media_attributes][1][item]=@/Users/shirakawatoshiaki/number/question_2.jpg' \
  # -F 'content[media_attributes][1][_destroy]=' \
  # -F 'content[media_attributes][2][id]=3' \
  # -F 'content[media_attributes][2][title]=file3!!' \
  # -F 'content[media_attributes][2][item]=@/Users/shirakawatoshiaki/number/question_3.jpg' \
  # -F 'content[media_attributes][2][_destroy]=1' \

  before_action :set_content, only: [:show, :update, :destroy]

  def index
		@contents = Content.documents.where(user: current_user).page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  # ユーザに公開されているコンテンツを取得
  def users_list
		@contents = current_user.contents.documents.release_contents.page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  def show
    render formats: 'json', handlers: 'jbuilder'
  end

  def create
    @content = Content.new(content_params)
    @content.item_type = Content.item_types[:document]
    @content.user_id = current_user.id
    @content.users = set_users
    @content.media.each do |m|
			m.encode_status = Medium.encode_statuses[:complete]
		end
    @content.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def update
    @content.update(content_params)
    @content.users = set_users
    @content.save!
    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @content.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

private
  def set_content
    @content = Content.find(params[:id])
  end

  def content_params
    params
      .require(:content)
      .permit(
        :title,
        :body,
        :released_at,
        :stopped_at,
        media_attributes: [:id, :title, :item, :_destroy],
    )
  end

  def set_users
    users = []
    return users if params[:user_ids].nil?
    params[:user_ids].each do |user_id|
      user = User.find(user_id.to_i)
      users << user
    end
    users
  end
end

