class Api::MovieUploadsController < AuthenticationController
  before_action :set_content, only: [:show, :update, :destroy]

  def index
		@contents = Content.movies.where(user: current_user).page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end
  
  # ユーザに公開されているコンテンツを取得
  def users_list
		@contents = current_user.contents.movies.release_contents.page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  def show
    render formats: 'json', handlers: 'jbuilder'
  end 

  def create
    @content = Content.new(content_params)
    @content.item_type = Content.item_types[:movie]
    @content.user_id = current_user.id
    @content.users = set_users
    @content.media.each do |m|
			m.encode_status = Medium.encode_statuses[:wait]
		end
		@content.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def update
    @content.update(content_params)
    @content.users = set_users
    @content.save!
    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @content.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

private

	def set_content
		@content = Content.find(params[:id])
	end

	def content_params
		params
			.require(:content)
			.permit(
				:title,
				:body,
				:released_at,
				:stopped_at,
				media_attributes: [:id, :title, :item, :_destroy]
		)
	end

  def set_users
    users = []
    return users if params[:user_ids].nil?
    params[:user_ids].each do |user_id|
      user = User.find(user_id.to_i)
      users << user
    end
    users
  end
end

