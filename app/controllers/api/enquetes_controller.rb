class Api::EnquetesController < AuthenticationController
  # -- create
  # curl -XPOST -v http://localhost:3000/api/enquetes \
  # -F 'user_ids[]=1' \
  # -F 'user_ids[]=2' \
  # -F 'content[title]=日本語アンケート' \
  # -F 'content[body]=どうでしょう' \
  # -F 'content[released_at]=2017-02-04 20:00:00' \
  # -F 'content[stopped_at]=2018-02-10 00:00:00' \
  # -F 'content[questions_attributes][0][id]=' \
  # -F 'content[questions_attributes][0][title]=質問1' \
  # -F 'content[questions_attributes][0][body]=選択式です' \
  # -F 'content[questions_attributes][0][question_type]=1' \
  # -F 'content[questions_attributes][0][_destroy]=' \
  # -F 'content[questions_attributes][0][choices_attributes][0][id]=' \
  # -F 'content[questions_attributes][0][choices_attributes][0][sentence]=どれ' \
  # -F 'content[questions_attributes][0][choices_attributes][0][free_flag]=0' \
  # -F 'content[questions_attributes][0][choices_attributes][0][_destroy]=' \
  # -F 'content[questions_attributes][0][choices_attributes][1][id]=' \
  # -F 'content[questions_attributes][0][choices_attributes][1][sentence]=だれ' \
  # -F 'content[questions_attributes][0][choices_attributes][1][free_flag]=0' \
  # -F 'content[questions_attributes][0][choices_attributes][1][_destroy]=' \
  # -F 'content[questions_attributes][1][id]=' \
  # -F 'content[questions_attributes][1][title]=問題2' \
  # -F 'content[questions_attributes][1][body]=複数回答可です' \
  # -F 'content[questions_attributes][1][question_type]=2' \
  # -F 'content[questions_attributes][1][_destroy]=' \
  # -F 'content[questions_attributes][1][choices_attributes][0][id]=' \
  # -F 'content[questions_attributes][1][choices_attributes][0][sentence]=あれ' \
  # -F 'content[questions_attributes][1][choices_attributes][0][free_flag]=0' \
  # -F 'content[questions_attributes][1][choices_attributes][0][_destroy]=' \
  # -F 'content[questions_attributes][1][choices_attributes][1][id]=' \
  # -F 'content[questions_attributes][1][choices_attributes][1][sentence]=これ' \
  # -F 'content[questions_attributes][1][choices_attributes][1][free_flag]=0' \
  # -F 'content[questions_attributes][1][choices_attributes][1][_destroy]=' \
  # -F 'content[questions_attributes][1][choices_attributes][2][id]=' \
  # -F 'content[questions_attributes][1][choices_attributes][2][sentence]=それ' \
  # -F 'content[questions_attributes][1][choices_attributes][2][free_flag]=0' \
  # -F 'content[questions_attributes][1][choices_attributes][2][_destroy]=' \
  # -F 'content[questions_attributes][2][id]=' \
  # -F 'content[questions_attributes][2][title]=問題3' \
  # -F 'content[questions_attributes][2][body]=複数回答可です' \
  # -F 'content[questions_attributes][2][question_type]=2' \
  # -F 'content[questions_attributes][2][_destroy]=' \
  # -F 'content[questions_attributes][2][choices_attributes][0][id]=' \
  # -F 'content[questions_attributes][2][choices_attributes][0][sentence]=選択肢1' \
  # -F 'content[questions_attributes][2][choices_attributes][0][free_flag]=0' \
  # -F 'content[questions_attributes][2][choices_attributes][0][_destroy]=' \
  # -F 'content[questions_attributes][2][choices_attributes][1][id]=' \
  # -F 'content[questions_attributes][2][choices_attributes][1][sentence]=選択肢2' \
  # -F 'content[questions_attributes][2][choices_attributes][1][free_flag]=0' \
  # -F 'content[questions_attributes][2][choices_attributes][1][_destroy]=' \
  # -F 'content[questions_attributes][2][choices_attributes][2][id]=' \
  # -F 'content[questions_attributes][2][choices_attributes][2][sentence]=選択肢3' \
  # -F 'content[questions_attributes][2][choices_attributes][2][free_flag]=0' \
  # -F 'content[questions_attributes][2][choices_attributes][2][_destroy]=' \
  # -F 'content[questions_attributes][2][choices_attributes][3][id]=' \
  # -F 'content[questions_attributes][2][choices_attributes][3][sentence]=自由記入欄' \
  # -F 'content[questions_attributes][2][choices_attributes][3][free_flag]=1' \
  # -F 'content[questions_attributes][2][choices_attributes][3][_destroy]=' \

  # -- answer
  # curl -XPOST -v http://localhost:3000/api/enquetes/answer \
  # -F 'answer_history[content_id]=406' \
  # -F 'answer_history[choices_attributes][0][choice_id]=457' \
  # -F 'answer_history[choices_attributes][0][free_word]=' \
  # -F 'answer_history[choices_attributes][1][choice_id]=458' \
  # -F 'answer_history[choices_attributes][1][free_word]=' \
  # -F 'answer_history[choices_attributes][2][choice_id]=459' \
  # -F 'answer_history[choices_attributes][2][free_word]=' \
  # -F 'answer_history[choices_attributes][3][choice_id]=461' \
  # -F 'answer_history[choices_attributes][3][free_word]=' \
  # -F 'answer_history[choices_attributes][4][choice_id]=462' \
  # -F 'answer_history[choices_attributes][4][free_word]=フリーダム' \

  before_action :set_content, only: [:show, :update, :destroy]

  def index
		@contents = Content.enquetes.where(user: current_user).page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
	end

  # ユーザに公開されているコンテンツを取得
  def users_list
		@contents = current_user.contents.enquetes.release_contents.page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  # 回答内容を受け取って登録を行う
  def answer
    # アンケートを取得
    content = Content.find(answer_params[:content_id])

    # 回答履歴を取得
    answer_histories = AnswerHistory.where(user: current_user).where(content_id: answer_params[:content_id])

    AnswerHistory.transaction do
      @answer_history = AnswerHistory.find_or_create_by(content: content, user: current_user)
      @answer_history.user = current_user
      @answer_history.content = content
      @answer_history.sequence = 1
      @answer_history.save!

      answer_params[:choices_attributes].each do |choice_attribute|
        choice_attr = choice_attribute

        begin
          choice = Choice.find(choice_attr[:choice_id])
        rescue
          logger.debug "not found choice_id: #{choice_attr[:choice_id]}"
          next
        end

        @answer_choice = AnswerChoice.new()
        @answer_choice.answer_history = @answer_history
        @answer_choice.question = choice.question
        @answer_choice.choice = choice
        @answer_choice.free_word = choice_attr[:free_word]
        @answer_choice.save!
      end
    end

    render formats: 'json', handlers: 'jbuilder'
  end

  # アンケートに回答した人を集計
  def aggregate
    @questions = []

    content = Content.find(params[:content_id])
    content.questions.each do |question|
      question_data = {
        question: question,
        choices: []
      }
      question.choices.each do |choice|
        choice_data = {
          choice: choice,
          answer_count: choice.answer_choices.count,
          free_words: []
        }

        if choice.free_flag
          choice.answer_choices.each do |answer_choice|
            free_word_data = {
              id: answer_choice.id,
              user_id: answer_choice.answer_history.user_id,
              name: answer_choice.answer_history.user.name,
              free_word: answer_choice.free_word
            }

            choice_data[:free_words] << free_word_data
          end
        end

        question_data[:choices] << choice_data
      end

      @questions << question_data
    end

    render formats: 'json', handlers: 'jbuilder'
  end

  def show
    render formats: 'json', handlers: 'jbuilder'
  end

  def create
    @content = Content.new(content_params)
    @content.item_type = Content.item_types[:enquete]
    @content.user_id = current_user.id
    @content.users = set_users
    @content.media.each do |m|
			m.encode_status = Medium.encode_statuses[:complete]
		end
    @content.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def update
    @content.update(content_params)
    @content.users = set_users
    @content.save!
    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @content.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

	def form
		#編集画面用
		#宛先
		@content = User.all
	end

  private
    def set_content
      @content = Content.find(params[:id])
    end

    def content_params
      params
        .require(:content)
        .permit(
          :title,
          :body,
          :clear_score,
          :perfect_score,
          :released_at,
          :stopped_at,
          questions_attributes: [
            :id, :title, :body, :question_type, :test_score, :_destroy,
            choices_attributes: [
              :id, :sentence, :free_flag, :answer_flag, :_destroy,
            ]
          ],
        )
    end

    def answer_params
      params
        .require(:answer_history)
        .permit(
          :content_id,
          choices_attributes: [
            :choice_id,
            :free_word,
          ]
        )
    end
  
    def set_users
      users = []
      return users if params[:user_ids].nil?
      params[:user_ids].each do |user_id|
        user = User.find(user_id.to_i)
        users << user
      end
      users
    end
end
