class Api::WebTestsController < AuthenticationController
  # -- create
  # curl -XPOST -v http://localhost:3000/api/web_tests \
  # -F 'user_ids[]=1' \
  # -F 'user_ids[]=2' \
  # -F 'content[title]=TITLE' \
  # -F 'content[body]=BODY' \
  # -F 'content[perfect_score]=100' \
  # -F 'content[clear_score]=70' \
  # -F 'content[released_at]=2017-02-04 20:00:00' \
  # -F 'content[stopped_at]=2018-02-10 00:00:00' \
  # -F 'content[questions_attributes][0][id]=' \
  # -F 'content[questions_attributes][0][title]=Question1' \
  # -F 'content[questions_attributes][0][body]=Who r u?' \
  # -F 'content[questions_attributes][0][question_type]=1' \
  # -F 'content[questions_attributes][0][test_score]=60' \
  # -F 'content[questions_attributes][0][_destroy]=' \
  # -F 'content[questions_attributes][0][choices_attributes][0][id]=' \
  # -F 'content[questions_attributes][0][choices_attributes][0][sentence]=I am.' \
  # -F 'content[questions_attributes][0][choices_attributes][0][free_flag]=0' \
  # -F 'content[questions_attributes][0][choices_attributes][0][answer_flag]=1' \
  # -F 'content[questions_attributes][0][choices_attributes][0][_destroy]=' \
  # -F 'content[questions_attributes][0][choices_attributes][1][id]=' \
  # -F 'content[questions_attributes][0][choices_attributes][1][sentence]=?' \
  # -F 'content[questions_attributes][0][choices_attributes][1][free_flag]=0' \
  # -F 'content[questions_attributes][0][choices_attributes][1][answer_flag]=0' \
  # -F 'content[questions_attributes][0][choices_attributes][1][_destroy]=' \
  # -F 'content[questions_attributes][1][id]=' \
  # -F 'content[questions_attributes][1][title]=Question2' \
  # -F 'content[questions_attributes][1][body]=What is this?' \
  # -F 'content[questions_attributes][1][question_type]=1' \
  # -F 'content[questions_attributes][1][test_score]=40' \
  # -F 'content[questions_attributes][1][_destroy]=' \
  # -F 'content[questions_attributes][1][choices_attributes][0][id]=' \
  # -F 'content[questions_attributes][1][choices_attributes][0][sentence]=cat.' \
  # -F 'content[questions_attributes][1][choices_attributes][0][free_flag]=0' \
  # -F 'content[questions_attributes][1][choices_attributes][0][answer_flag]=1' \
  # -F 'content[questions_attributes][1][choices_attributes][0][_destroy]=' \
  # -F 'content[questions_attributes][1][choices_attributes][1][id]=' \
  # -F 'content[questions_attributes][1][choices_attributes][1][sentence]=?' \
  # -F 'content[questions_attributes][1][choices_attributes][1][free_flag]=0' \
  # -F 'content[questions_attributes][1][choices_attributes][1][answer_flag]=0' \
  # -F 'content[questions_attributes][1][choices_attributes][1][_destroy]=' \

  # -- answer
  # curl -XPOST -v http://localhost:3000/api/web_tests/answer \
  # -F 'answer_history[content_id]=406' \
  # -F 'answer_history[choices_attributes][0][choice_id]=457' \
  # -F 'answer_history[choices_attributes][0][free_word]=' \
  # -F 'answer_history[choices_attributes][1][choice_id]=458' \
  # -F 'answer_history[choices_attributes][1][free_word]=' \
  # -F 'answer_history[choices_attributes][2][choice_id]=459' \
  # -F 'answer_history[choices_attributes][2][free_word]=' \
  # -F 'answer_history[choices_attributes][3][choice_id]=461' \
  # -F 'answer_history[choices_attributes][3][free_word]=' \
  # -F 'answer_history[choices_attributes][4][choice_id]=462' \
  # -F 'answer_history[choices_attributes][4][free_word]=' \

  # -- users_histories
  # curl -XGET -v http://localhost:3000/api/web_tests/406/users_histories
  #   parameter: content_id

  # -- aggregate
  # curl -XGET -v http://localhost:3000/api/web_tests/aggregate/406 
  #   parameter: content_id

  before_action :set_content, only: [:show, :update, :destroy]

  def index
		@contents = Content.tests.where(user: current_user).page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
	end

  # ユーザに公開されているコンテンツを取得
  def users_list
		@contents = current_user.contents.tests.release_contents.page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  # 回答内容を受け取って採点を行う
  def answer
    # 採点対象のテストを取得
    content = Content.find(answer_params[:content_id])

    # 回答履歴を取得
    answer_histories = AnswerHistory.where(user: current_user).where(content_id: answer_params[:content_id])

    AnswerHistory.transaction do
      @answer_history = AnswerHistory.new()
      @answer_history.user = current_user
      @answer_history.content = content
      @answer_history.sequence = answer_histories.count + 1
      @answer_history.score = scoring content
      @answer_history.pass_flag = (@answer_history.score >= content.clear_score)
      @answer_history.save!

      answer_params[:choices_attributes].each do |choice_attribute|
        choice_attr = choice_attribute

        begin
          choice = Choice.find(choice_attr[:choice_id])
        rescue
          logger.debug "not found choice_id: #{choice_attr[:choice_id]}"
          next
        end

        @answer_choice = AnswerChoice.new()
        @answer_choice.answer_history = @answer_history
        @answer_choice.question = choice.question
        @answer_choice.choice = choice
        @answer_choice.free_word = choice_attr[:free_word]
        @answer_choice.save!
      end
    end

    render formats: 'json', handlers: 'jbuilder'
  end

  # 自分が回答したテストの履歴を取得する
  def users_histories
    @answer_histories = current_user.answer_histories.where(content_id: params[:id])
    render formats: 'json', handlers: 'jbuilder'
  end

  # テストに回答した人の最高得点を集計
  def high_score
    @answer_users = []

    content = Content.find(params[:content_id])
    content.users.each do |user|
      answer_user = {
        user: user,
        answer_history_id: nil,
        score: nil,
        tested_at: nil
      }

      max_answer = AnswerHistory.where(content_id: params[:content_id]).where(user_id: user.id).order('score DESC').limit(1).first

      unless max_answer.nil?
        answer_user = {
          user: user,
          answer_history_id: max_answer.id,
          score: max_answer.score,
          tested_at: max_answer.created_at
        }
      end

      @answer_users << answer_user
    end
    render formats: 'json', handlers: 'jbuilder'
  end

  def aggregate
    @questions = []

    content = Content.find(params[:content_id])
    content.questions.each do |question|
      question_data = question.attributes.symbolize_keys
      question_data[:total_score] = 0
      question_data[:average] = 0
      question_data[:correct_rate] = 0
      question_data[:correct_count] = 0
      question_data[:users_count] = 0
      question_data[:choices] = []

      question.choices.each do |choice|
        choice_data = choice.attributes.symbolize_keys
        choice_data[:select_count] = 0
        choice_data[:choice_users] = []

        question_data[:choices] << choice_data
      end

      @questions << question_data
    end

    content.users.each do |user|
      latest_history = AnswerHistory.where(content_id: params[:content_id]).where(user_id: user.id).order('sequence DESC').limit(1).first
      
      next if latest_history.blank?

      latest_history.web_test_results.each do |question_result|
        @questions.map! {|question|
          if question[:id] == question_result[:question_id]
            question[:correct_count] += 1 if question_result[:judge]
            question[:total_score] += question_result[:score]
            question[:users_count] += 1
            question[:choices].map! {|choice|
              question_result[:choices].each do |choice_result|
                if choice[:id] == choice_result.choice.id
                  choice[:select_count] += 1
                  choice[:choice_users] << {id: user.id, name: user.name, free_word: choice_result.free_word}
                end
              end
              choice
            }
          end
          question
        }
      end
    end

    @questions.map! {|question|
      question[:average_score] = question[:total_score] / question[:users_count] if question[:users_count] > 0
      question[:correct_rate] = question[:correct_count].to_f / question[:users_count].to_f * 100.0 if question[:users_count] > 0
      question
    }

    logger.debug @questions

    render formats: 'json', handlers: 'jbuilder'
  end

  def show
    render formats: 'json', handlers: 'jbuilder'
  end

  def create
    @content = Content.new(content_params)
    @content.item_type = Content.item_types[:test]
    @content.user_id = current_user.id
    @content.users = set_users
    @content.media.each do |m|
			m.encode_status = Medium.encode_statuses[:complete]
		end
    @content.save!
    render formats: 'json', handlers: 'jbuilder'
  end

  def update
    @content.update(content_params)
    @content.users = set_users
    @content.save!
    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @content.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

	def form
		#編集画面用
		#宛先
		@content = User.all
	end

  private
    def set_content
      @content = Content.find(params[:id])
    end
  
    def content_params
      params
        .require(:content)
        .permit(
          :title,
          :body,
          :clear_score,
          :perfect_score,
          :released_at,
          :stopped_at,
          questions_attributes: [
            :id, :title, :body, :question_type, :test_score, :_destroy,
            choices_attributes: [
              :id, :sentence, :free_flag, :answer_flag, :_destroy,
            ]
          ],
        )
    end
  
    def answer_params
      params
        .require(:answer_history)
        .permit(
          :content_id,
          choices_attributes: [
            :choice_id,
            :free_word,
          ]
        )
    end
  
    def set_users
      users = []
      return users if params[:user_ids].nil?
      params[:user_ids].each do |user_id|
        user = User.find(user_id.to_i)
        users << user
      end
      users
    end
  
    def scoring content
      score = 0
  
      content.questions.each do |question|
        logger.debug "scoring question_id: #{question.id}"
  
        case question.question_type
        when Question.question_types[:select_one]
          score += scoring_select_one question
        when Question.question_types[:multi_select]
          score += scoring_multi_select question
        end
      end
  
      score
    end
  
    def scoring_select_one question
      logger.debug "scoring_select_one question_id: #{question.id}"
  
      correct_flag = false
      score = 0
  
      # 正解の選択肢を取得
      correct_choice = question.choices.where(answer_flag: true).first
      logger.debug "correct choice_id: #{correct_choice.id}"
  
      # 正解の選択肢のIDと同じIDが入力されていれば正解
      answer_params[:choices_attributes].each do |choice_attribute|
        choice_attr = choice_attribute
  
        logger.debug "## attributes choice_id: #{choice_attr[:choice_id]}"
        if correct_choice.id == choice_attr[:choice_id]      
          logger.debug "## correct!"
          correct_flag = true 
          break
        end
      end
  
      if correct_flag
        score = question.test_score
      end
  
      score
    end
  
    def scoring_multi_select question
      logger.debug "scoring_multi_select question_id: #{question.id}"
  
      correct_flag = true
      score = 0
      selected_choice_ids = []
  
      # 正解の選択肢を取得
      correct_choices = question.choices.where(answer_flag: true)
  
      # 選択された選択肢のIDを抽出
      answer_params[:choices_attributes].each do |choices|
        selected_choice_ids << choices[:choice_id]
      end
  
      # 選択された選択肢のchoicesを取得
      select_choices = question.choices.where(id: selected_choice_ids)
  
      # 不正解の選択肢を選択している場合は不正解
      select_choices.each do |select_choice|
        logger.debug "## select_choice choice_id: #{select_choice.id} correct: #{select_choice.answer_flag}"
        unless select_choice.answer_flag
          logger.debug "## incorrect..."
          correct_flag = false
          break
        end
      end
  
      logger.debug "correct_choices count: #{correct_choices.count}  select_choices: #{select_choices.count}"
  
      # 正解選択肢を選んでいない場合は不正解
      if correct_choices.count != select_choices.count
        logger.debug "## incorrect..!"
        correct_flag = false
      end
  
      if correct_flag
        score = question.test_score
      end
  
      score
    end
end

