class Api::BoardCommentsController < AuthenticationController
  # -- board create
  # curl -XPOST -v -H "Access-Token: $ACCESS_TOKEN" -H "Client: $CLIENT" -H "Uid: $UID_MAIL" http://localhost:3000/api/boards/:board_id/board_comments \
  # -F 'board[comment]=COMMENT' \

  before_action :set_board_comment, only: [:destroy]

  def create
    @board = BoardComment.new(comment_params)
    @board.board_id = params[:board_id]
    @board.user_id = current_user.id
    @board.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @board.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

private
  def set_board_comment
    @comment = BoardComment.find(params[:id])
  end

  def comment_params
    params
      .require(:board_comment)
      .permit(
        :comment
    )
  end
end
