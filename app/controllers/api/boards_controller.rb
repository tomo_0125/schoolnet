class Api::BoardsController < AuthenticationController
  # -- board create
  # curl -XPOST -v -H "Access-Token: $ACCESS_TOKEN" -H "Client: $CLIENT" -H "Uid: $UID_MAIL" http://localhost:3000/api/boards \
  # -F 'board[title]=TITLE' \
  # -F 'board[body]=BODY' \

  # -- board update
  # curl -XPUT -v -H "Access-Token: $ACCESS_TOKEN" -H "Client: $CLIENT" -H "Uid: $UID_MAIL" http://localhost:3000/api/boards/10 \
  # -F 'board[title]=TITLE' \
  # -F 'board[body]=BODY' \

  before_action :set_board, only: [:show, :update, :destroy]

  def index
    @boards = Board.page(params[:page])
    render formats: 'json', handlers: 'jbuilder'
  end

  def show
    render formats: 'json', handlers: 'jbuilder'
  end

  def create
    @board = Board.new(board_params)
    @board.user_id = current_user.id
    @board.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def update
    @board.update(board_params)
    @board.save!

    render formats: 'json', handlers: 'jbuilder'
  end

  def destroy
    @board.destroy
    render formats: 'json', handlers: 'jbuilder'
  end

private
  def set_board
    @board = Board.find(params[:id])
  end

  def board_params
    params
      .require(:board)
      .permit(
        :title,
        :body
    )
  end
end
