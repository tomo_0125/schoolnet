class Api::UserListController < AuthenticationController
  def index
    @resources = User.where(school_id: current_user.school_id)
    render json: @resources
  end

  private
end
