class SessionsController < DeviseTokenAuth::SessionsController
	def create
		# Check
		fields = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys)

		@resource = nil
		if fields.present? && fields.length >= 2
			q_value1 = resource_params[fields[0]]

			if resource_class.case_insensitive_keys.include?(fields[0])
				q_value1.downcase!
			end

			q_value2 = resource_params[fields[1]]

			if resource_class.case_insensitive_keys.include?(fields[1])
				q_value2.downcase!
			end

			q = "#{fields[0].to_s} = ? AND #{fields[1].to_s} = ?"

			if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
				q = "BINARY " + q
			end

			@resource = resource_class.where(q, q_value1, q_value2).first
		else
			render_create_error_bad_credentials
			return
		end

		if @resource and valid_params?(fields[0], q_value1) and valid_params?(fields[1], q_value2) and (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
			valid_password = @resource.valid_password?(resource_params[:password])
			if (@resource.respond_to?(:valid_for_authentication?) && !@resource.valid_for_authentication? { valid_password }) || !valid_password
				render_create_error_bad_credentials
				return
			end
			# create client id
			@client_id = SecureRandom.urlsafe_base64(nil, false)
			@token     = SecureRandom.urlsafe_base64(nil, false)

      p "####"
      p @token
      p "####"

			@resource.tokens[@client_id] = {
				token: BCrypt::Password.create(@token),
				expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
			}
			@resource.save

			sign_in(:user, @resource, store: false, bypass: false)

			yield @resource if block_given?

			render_create_success
		elsif @resource and not (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
			render_create_error_not_confirmed
		else
			render_create_error_bad_credentials
		end
	end

	def render_create_success
		render json: {
			data: resource_data(resource_json: @resource.to_response)
		}
	end
end
