class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  skip_before_filter :verify_authenticity_token

	before_filter :set_access_control_headers
	before_filter :configure_permitted_parameters, if: :devise_controller?

  def set_access_control_headers
  end

	def configure_permitted_parameters
    params_for_resource(:sign_in) { |u| u.permit(:school_id, :login_id, :password, :remember_me) }
	end
end
