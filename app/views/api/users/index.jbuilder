json.users do |json|
  json.array!(@users) do |user|
    json.id user.id
    json.email user.email
    json.name user.name
    json.nickname user.nickname
    json.created_at user.created_at
  end
end

