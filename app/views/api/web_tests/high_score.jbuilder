json.array!(@answer_users) do |answer_user|
  json.partial! 'api/web_tests/high_score_answer_user', answer_user: answer_user
end

