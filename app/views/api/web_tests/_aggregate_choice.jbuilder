json.id choice[:id]
json.sentence choice[:sentence]
json.free_flag choice[:free_flag]
json.answer_flag choice[:answer_flag]
json.choice_users choice[:choice_users] do |choice_user|
  json.partial! 'api/web_tests/aggregate_choice_user', choice_user: choice_user
end
