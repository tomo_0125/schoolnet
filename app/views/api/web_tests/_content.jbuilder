#とりあえず全部
json.id content.id
json.user_id content.user_id
json.title content.title
json.body content.body
json.item_type content.item_type
json.file_extension content.file_extension
json.content_type content.content_type
json.play_time content.play_time
json.clear_score content.clear_score
json.perfect_score content.perfect_score
json.released_at content.released_at
json.stopped_at content.stopped_at
json.created_at content.created_at
json.updated_at content.updated_at
json.deleted_at content.deleted_at
json.questions_attributes content.questions do |question|
  json.partial! 'api/web_tests/question', question: question
end
#debug用
json.debug_template 'jbuilder'
