json.array!(@answer_histories) do |answer_history|
  json.partial! 'api/web_tests/answer_history', answer_history: answer_history
end

