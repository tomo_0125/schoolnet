json.partial! 'api/web_tests/user', user: answer_user[:user]
json.id answer_user[:answer_history_id]
json.score answer_user[:score]
json.tested_at answer_user[:tested_at]
