json.array!(@questions) do |question|
  json.partial! 'api/web_tests/aggregate_question', question: question
end

