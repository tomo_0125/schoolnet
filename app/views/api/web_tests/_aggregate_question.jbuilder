json.id question[:id]
json.title question[:title]
json.body question[:body]
json.question_type question[:question_type]
json.correct_count question[:correct_count]
json.users_count question[:users_count]
json.average_score question[:average_score]
json.correct_rate question[:correct_rate]
json.choices question[:choices] do |choice|
  json.partial! 'api/web_tests/aggregate_choice', choice: choice
end
