json.array!(@contents) do |content|
  json.partial! 'api/web_tests/content', content: content
end

