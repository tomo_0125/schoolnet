json.array!(@contents) do |content|
  json.partial! 'api/movie_uploads/content', content: content
end

