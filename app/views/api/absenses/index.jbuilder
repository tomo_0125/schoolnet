json.array!(@contents) do |content|
  json.partial! 'api/file_uploads/content', content: content
end

