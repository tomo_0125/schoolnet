json.cook_menus do |json|
  json.array!(@cook_menus) do |cook_menu|
    json.id cook_menu.id
    json.date "#{I18n.l(cook_menu.date, format: :long)}(#{%w(日 月 火 水 木 金 土)[cook_menu.date.wday]})"
    json.event cook_menu.event
    json.menu cook_menu.menu
    json.ingredients cook_menu.ingredients
    json.snack cook_menu.snack
  end
end

