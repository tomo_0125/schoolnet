#とりあえず全部
json.id board.id
json.user_id board.user_id
json.title board.title
json.body board.body
json.created_at board.created_at
json.updated_at board.updated_at
json.deleted_at board.deleted_at
json.board_comments board.board_comments do |board_comment|
  json.partial! 'api/boards/board_comment', board_comment: board_comment
end
#debug用
json.debug_template 'jbuilder'
