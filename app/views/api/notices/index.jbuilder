json.notices do |json|
  json.array!(@notices) do |notice|
    json.id notice.id
    json.title notice.title
    json.date I18n.l notice.date
    json.url notice.url
    if notice.thumbnail.exists?
      json.thumbnail notice.thumbnail.url
    else
      json.thumbnail ""
    end
  end
end


