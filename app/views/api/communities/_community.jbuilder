#とりあえず全部
json.id community.id
json.user_id community.user_id
json.user_name community.user.name
json.title community.title
json.body community.body
json.join_status community.join_status(current_user)
json.limited community.limited
json.created_at community.created_at
json.updated_at community.updated_at
json.deleted_at community.deleted_at
json.community_comments community.community_comments do |community_comment|
  json.partial! 'api/communities/community_comment', community_comment: community_comment
end
#debug用
json.debug_template 'jbuilder'
