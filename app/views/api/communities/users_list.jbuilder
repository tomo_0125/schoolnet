json.array!(@communities) do |community|
  json.partial! 'api/communities/community', community: community
end

