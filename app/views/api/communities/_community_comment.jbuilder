json.id community_comment.id
json.user_id community_comment.user_id
json.user_name community_comment.user.name
json.comment community_comment.comment
json.created_at community_comment.created_at
