json.id choice[:choice].id
json.sentence choice[:choice].sentence
json.count choice[:answer_count]
json.free_flag choice[:choice].free_flag
json.free_words choice[:free_words] do |free_word|
  json.partial! 'api/enquetes/aggregate_free_word', free_word: free_word
end
