json.id question.id
json.title question.title
json.body question.body
json.question_type question.question_type
json.test_score question.test_score
json.choices_attributes question.choices do |choice|
  json.partial! 'api/enquetes/choice', choice: choice
end
