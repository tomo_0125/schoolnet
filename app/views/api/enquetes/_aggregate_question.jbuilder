json.id question[:question].id
json.title question[:question].title
json.body question[:question].body
json.question_type question[:question].question_type
json.choices question[:choices] do |choice|
  json.partial! 'api/enquetes/aggregate_choice', choice: choice
end
