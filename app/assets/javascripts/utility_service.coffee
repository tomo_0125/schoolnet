# Place aml the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
.factory('UtilityService', [ () ->
	return {
		resouce_to_formdata: (rsc) ->
			fd = new FormData()

			add = (prefix, item) ->
				if (item instanceof File)
					fd.append(prefix, item)
				else if (angular.isArray(item) || angular.isObject(item))
					 angular.forEach(item, (val, key) ->
						 if (String(key).indexOf('$') == 0)
								return
							add(prefix +  '[' + key + ']', val)
					)
				else
					fd.append(prefix, item)

			angular.forEach(rsc, (obj, prefix) ->
				add(prefix, obj)
			)

			return fd

		get_filename: (str, defaultValue = '') ->
			if (angular.isUndefined(str) || !angular.isString(str))
				return defaultValue

			parse = str.match(".+/(.+?)([\?#;].*)?$")
			if (parse == null)
				return defaultValue

			return parse[1]
	}
])

