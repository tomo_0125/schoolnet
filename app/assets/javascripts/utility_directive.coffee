# Place aml the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
# directive prefix 'sn-', sn = school net
.directive('snUiInputFile', () ->
	return {
		restrict: 'A',
		priority: 0,
		scope: false,
		multiElement: false,
		link: (scope, elmt, attr) ->
			elmt.find('#date_test').datetimepicker()

			# input file hidden
			elmt.find(':input[type="file"]').hide()

			# append view items
			view = angular.element('<div />')
			view
				.addClass('input-group')
				.appendTo(elmt)
			# filename view input:text
			angular
				.element('<input />')
			  .attr({
					type: 'text',
					name: 'upload-view',
					placeholder: 'select files...'
				})
				.prop('readonly', true)
				.addClass('form-control sn-ui-opendir')
				.appendTo(view)
			# open dialog button
			angular
			  .element('<span />')
				.addClass('input-group-btn')
				.append(
					angular
						.element('<button />')
						.attr('name', 'upload-opendlg')
						.addClass('btn btn-primary sn-ui-opendir')
					.append(
						angular
							.element('<i />')
							.addClass('glyphicon glyphicon-file')
					)
				)
				.appendTo(view)

			# events
			elmt
				.find('.sn-ui-opendir')
					.on('click', (e) ->
						elmt.find(':input[type="file"]:first').click()
					)
				.end()
				.find(':input[type="file"]:first')
					.on('change', (e) ->
						elmt.find(':input[name="upload-view"]')
							.val(angular.element(e.currentTarget).val())
					)
	}
)
.directive('snUiDateTimePicker', () ->
	return {
		restrict: 'A',
		priority: 0,
		scope: false,
		multiElement: false,
		link: (scope, elmt, attr) ->
			elmt.addClass('sn-ui-datepicker')

			# datepicker format
			fmt = 'YYYY年 MM月 DD日 HH:mm:ss'

			# original input
			org = elmt.find(':input[type="text"]')
			org
				.addClass('su-ui-datepicker-original')
				.hide()

			# user interface input
			dtp_ipt = angular.element('<input />')
			dtp_ipt
				.attr('type', 'text')
				.addClass('form-control view-input')

			# evetns
			elmt
				.on('apply_datepicker', () ->
					org.change()
				)
				.on('clear_datepicker', () ->
					dtp_ipt.val('')
				)
			org
				.on('change', (ev) ->
					mmt = moment(ev.currentTarget.value)
					if (mmt.isValid())
						dtp_ipt.val(mmt.format(fmt))
				)
			dtp_ipt
				.on('change blur focus', (ev) ->
					mmt = moment(ev.currentTarget.value, fmt)
					if (mmt.isValid())
						org.val(mmt.format('YYYY-MM-DDTHH:mm:ss')).change()
				)

			# build datepicker
			dtp = angular.element('<div />')
			dtp
				.addClass('input-group date')
				.append(dtp_ipt)
				.append(
					# calendar button
					angular
						.element('<span />')
						.addClass('input-group-addon')
						.append(
							angular
								.element('<span />')
								.addClass('glyphicon glyphicon-calendar')
						)
				)
				.datetimepicker({
					format: fmt,
					dayViewHeaderFormat: 'YYYY年 M月',
					locale: 'ja',
					showTodayButton: true,
					tooltips: {
						today: '今日',
						clear: 'クリア',
						close: '閉じる',
						selectMonth: '月を選択',
						prevMonth: '前月',
						nextMonth: '翌月',
						selectYear: '年を選択',
						prevYear: '前年',
						nextYear: '翌年',
						selectDecade: '年を選択',
						prevDecade: '前の10年',
						nextDecade: '次の10年',
						prevCentury: '前の100年',
						nextCentury: '次の100年'
					}
				})
				.appendTo(elmt)
	}
)
