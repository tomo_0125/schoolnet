# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
.factory('MovieUploadService', [ () ->
	return {
		medium_status: {
			new: 'new',
			uploaded: 'uploaded',
			reupload: 'reupload',
			encoding: 'encoding'
    },

		create_content: (assign) ->
			return angular.extend({
				media_attributes: [this.create_medium()]
			}, assign)

		create_medium: (assign) ->
			return angular.extend({
				title: '',
				local_status: this.medium_status.new
			}, assign)

		request_filter: (data) ->

			# optimize media array
			mstatus = this.medium_status
			marray = []
			angular.forEach(data.content.media_attributes, (media, idx) ->
				if (media.local_status == mstatus.new && !media.item instanceof File)
					return

				clone = angular.extend({}, media)
				if (clone.local_status == mstatus.uploaded)
					delete clone.item
				delete clone.local_status
				marray.push(clone)
			)
			data.content.media_attributes = marray

			return data

		response_filter: (data) ->
			data = angular.fromJson(data)
			mstatus = this.medium_status

			filter = (content) ->
				angular.forEach(content.media_attributes, (media, idx) ->
					media.local_status = mstatus.uploaded
				)
			if (angular.isArray(data))
				angular.forEach(data, (content, idx) ->
					filter(content)
				)
			else
				filter(data)

			return data
	}
])
.factory('MovieUploadResource', ['$resource', 'MovieUploadService', 'UtilityService', ($resource, MovieUploadService, UtilityService) ->
	return $resource('http://localhost:3000/api/movie_uploads/:id', {
			id: '@id'
		}, {
			query: {
				method: 'GET',
				isArray: true,
				transformResponse: (data, headers) ->
					return MovieUploadService.response_filter(data)
			},
			get: {
				isArray: false,
				transformResponse: (data, headers) ->
					return MovieUploadService.response_filter(data)
			},
			save: {
				method: 'POST',
				headers: {
					'Content-Type': undefined,
					enctype:'multipart/form-data'
				},
				transformRequest: (data, headersGetter) ->
					data = MovieUploadService.request_filter(data)
					return UtilityService.resouce_to_formdata(data, headersGetter)
			},
			update: {
				method: 'PATCH',
				headers: {
					'Content-Type': undefined,
					enctype:'multipart/form-data'
				},
				transformRequest: (data, headersGetter) ->
					data = MovieUploadService.request_filter(data)
					return UtilityService.resouce_to_formdata(data, headersGetter)
			}
		})
])
.controller('MovieUploadsController', ['$scope', '$log', '$uibModal', 'MovieUploadResource', 'UserListResource', 'MovieUploadService', 'UtilityService', ($scope,  $log, $uibModal, MovieUploadResource, UserListResource, MovieUploadService, UtilityService) ->

	mv_rsc = MovieUploadResource
	ul_rsc = UserListResource
	mv_svc = MovieUploadService

	$scope.action = (action, data, options) ->
		switch action
			when 'list_from'
				mv_rsc.query().$promise
				.then((res) ->
					$scope.list = res
					$scope.viewmode = 'list'
				)
			when 'list_to'
				console.log('list_to')
			when 'detail'
				mv_rsc.get({id: data.id}).$promise
				.then((res) ->
					$scope.content = res
					$scope.viewmode = 'detail'
				)
			when 'edit'
				ul_rsc.query().$promise
				.then((res) ->
					$scope.form.users = res
					if (data)
						mv_rsc.get({id:data.id}).$promise
						.then((res) ->
							console.log(res)
							$scope.content = mv_svc.create_content(res)
							angular.element(document).find('.sn-ui-datepicker').trigger('apply_datepicker')
							$scope.viewmode = 'edit'
						)
					else
						$scope.content = mv_svc.create_content()
						angular.element(document).find('.sn-ui-datepicker').trigger('clear_datepicker')
						$scope.viewmode = 'new'
				)
			when 'create'
				$log.debug($scope.content)
				mv_rsc.save({id:$scope.content.id}, {content:$scope.content}).$promise
				.then((res) ->
					$scope.action('list_from')
				)
			when 'update'
				$log.debug($scope.content)
				mv_rsc.update({id:$scope.content.id}, {content:$scope.content}).$promise
				.then((res) ->
					$scope.action('list_from')
				)
			when 'delete'
				mv_rsc.delete({id:data.id}).$promise
				.then((res) ->
					$scope.action('list_from')
				)
			when 'addfile'
				data.push({})
			when 'delfile'
				if (data.local_status == mv_svc.medium_status.uploaded ||
						 data.local_status == mv_svc.medium_status.reupload)
					data.destroy = 1
				else if (data.local_status == mv_svc.medium_status.new)
					$scope.content.media_attributes.splice(options.index, 1)

				cnt = 0
				angular.forEach($scope.content.media_attributes, (medium, idx) ->
					if (!medium.destroy)
						cnt++
				)
				if (cnt == 0)
					$scope.content.media_attributes.push(mv_svc.create_medium())
			when 'show_movie'
				console.log(data)
				$scope.movies = [{src: data}]
				$uibModal.open({
					templateUrl: 'movie-dialog',
					controller: 'MovieUploadsModalController'
					scope: $scope,
					backdrop: true,
					size: 'lg'
				})

	# utility
	$scope.util = {
		get_filename: (str) ->
			path = decodeURI(str)
			return UtilityService.get_filename(path)
	}

	$scope.form = {}
	$scope.modal = {}
	$scope.action('list_from')
])
.controller('MovieUploadsModalController', ['$scope', '$uibModalInstance', ($scope, $uibModalInstance) ->

	$scope.modal.action = (action) ->
		switch action
			when 'close'
				$uibModalInstance.close()

])
