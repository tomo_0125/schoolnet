# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
################################################################################
#        Type: Factory
#        Name: WebTestService
# Description: Webテストに関するビジネスロジックを提供する
#              (Viewに関係のないデータの加工, 設定の定義など)
################################################################################
.factory('WebTestService', ->
	return {
		getServiceEnv: ->
			return {
				questionTypes:
					selectOne: '1'
					multiSelect: '2'
				
				listTypes:
					from: 'from'
					to: 'to'
			}

		requestFilter: (data, headersGetter) ->
			if (angular.isArray(data.content.questions_attributes))
				data.content.questions_attributes =
					data.content.questions_attributes.filter((item, index, array) ->
						return (item.id || !item.destroy)
					)
			return angular.toJson(data)

		createAnswer: (content) ->
			data =
				answer_history:
					content_id: content.id
					choices_attributes: []

			angular.forEach(content.questions_attributes, (question, qidx) ->
				angular.forEach(question.choices_attributes, (choice, cidx) ->
					if (choice.selected)
						data.answer_history.choices_attributes.push(
							choice_id: choice.id
							free_word: ''
						)
				)
			)
			return data
	}
)
################################################################################
#        Type: Factory
#        Name: WebTestResource
# Description: RESTfulによるデータ受け渡しをラップして提供する
################################################################################
.factory('WebTestResource', ['$resource', 'WebTestService', ($resource, WebTestService) ->
	return $resource('http://localhost:3000/api/web_tests/:id', { id: '@id' },
		usersList:
			method: 'GET',
			# TODO: 同一ドメインに対してもプロトコルとドメインが必要な問題
			url: 'http://localhost:3000/api/web_tests/users_list'
			isArray: true

		answer:
			method: 'POST'
			url: 'http://localhost:3000/api/web_tests/answer'
			isArray: false

		save:
			method: 'POST'
			transformRequest: WebTestService.requestFilter
		
		update:
			method: 'PATCH'
			transformRequest: WebTestService.requestFilter
	)
])
################################################################################
#        Type: Controller
#        Name: WebTestsController
# Description: WebテストのCURD, リスト表示する機能を提供する
################################################################################
.controller('WebTestsController', ['$scope', '$log', '$q', '$uibModal', 'WebTestResource', 'WebTestService', ($scope, $log, $q, $uibModal, WebTestResource, WebTestService) ->

	wtRsc = WebTestResource
	wtSrv = WebTestService

	$scope.initialize = ->
		$scope.env = wtSrv.getServiceEnv()
		$scope.listMode = $scope.env.listTypes.from
		$scope.action.list()

	$scope.action = {}
	privateAction = {}

	# controller内だけに提供するaction
	privateAction = {}
	privateAction.listFrom = ->
		$log.debug('private action call: listFrom')
		dfd = $q.defer()
		wtRsc.query().$promise.then((data) ->
			$scope.list = data
			$scope.viewMode = 'list'
			dfd.resolve(data)
		)
		.catch((data) ->
			dfd.reject(data)
		)
		dfd.promise

	privateAction.listTo = ->
		$log.debug('private action call: listTo')
		dfd = $q.defer()
		wtRsc.usersList().$promise.then((data) ->
			$scope.list = data
			$scope.viewMode = 'list'
			dfd.resolve(data)
		)
		.catch((data) ->
			dfd.reject(data)
		)
		dfd.promise

	privateAction.initDatePicker = ->
		angular.element(document).find('.sn-ui-datepicker').trigger('apply_datepicker')

	# viewに提供するaction
	# テストの管理に関するaction
	$scope.action.list = (type) ->
		if (angular.isString(type))
			$scope.listMode = type
		$log.debug("action call: list (listMode: #{$scope.listMode})")
		switch $scope.listMode
			when $scope.env.listTypes.from
				action = privateAction.listFrom
			when $scope.env.listTypes.to
				action = privateAction.listTo
			else
				return
		$scope.list = {}
		$scope.listLoading = true
		action().finally(() ->
			$scope.listLoading = false
		)

	$scope.action.show = (id) ->
		$log.debug('action call: show')
		wtRsc.get({ id: id }).$promise.then((data) ->
			$scope.content = data
			$scope.viewMode = 'detail'
			$scope.showMode = 'summary'
		)

	$scope.action.detail = (showMode) ->
		$log.debug("action call: detail (#{showMode})")
		$scope.showMode = showMode

	$scope.action.new = ->
		$log.debug('action call: new')
		$scope.content = {}
		$scope.viewMode = 'edit'
		$scope.formMode = 'summary'

	$scope.action.edit = (id) ->
		$log.debug('action call: edit')
		wtRsc.get({ id: id }).$promise.then((data) ->
			$scope.content = data
			privateAction.initDatePicker()
			$scope.viewMode = 'edit'
			$scope.formMode = 'summary'
		)

	$scope.action.form = (formMode) ->
		$log.debug("action call: form (formMode:#{formMode})")
		$scope.formMode = formMode

	$scope.action.save = ->
		$log.debug('action call: save')
		wtRsc.save({ id: $scope.content.id }, { content: $scope.content }).$promise.then((res) ->
			$scope.action.list()
		)
	
	$scope.action.update = ->
		$log.debug('action call: update')
		wtRsc.update({ id: $scope.content.id }, { content: $scope.content }).$promise.then((res) ->
			$scope.action.list()
		)

	$scope.action.delete = (id) ->
		$log.debug('action call: delete')
		wtRsc.delete({ id: id }).$promise.then((res) ->
			$scope.action.list()
		)

	$scope.action.questionShow = (question) ->
		$log.debug('action call: questionShow')
		$uibModal.open(
			templateUrl: 'question-dialog-show'
			controller: 'WebTestsQuestionController'
			resolve:
				question: ->
					return question
			backdrop: 'static'
			size: 'lg'
		)

	$scope.action.questionEdit = (question) ->
		$log.debug('action call: questionEdit')
		$uibModal.open(
			templateUrl: 'question-dialog-edit'
			controller: 'WebTestsQuestionController'
			resolve:
				question: ->
					return question
			backdrop: 'static'
			size: 'lg'
		)

	$scope.action.questionAppend =  ->
		$log.debug('action call: questionAppend')
		if (!angular.isArray($scope.content.questions_attributes))
			$scope.content.questions_attributes = []
		$scope.content.questions_attributes.push(
			question_type: $scope.env.questionTypes.selectOne
		)

	$scope.action.questionDelete = (question) ->
		$log.debug('action call: questionDelete')
		question.destroy = 1

	# テストに関するaction
	$scope.action.test = (id) ->
		$log.debug('action call: test')
		wtRsc.get({ id: id }).$promise.then((data) ->
			$scope.content = data
			$scope.test =
				currentIndex: 0
				maxIndex: data.questions_attributes.length - 1
				question: data.questions_attributes[0]
				view: 'question'
			$scope.viewMode = 'test'
		)

	# boçot controller
	$scope.initialize()
])
################################################################################
#        Type: Controller
#        Name: WebTestsQuestionController
# Description: 設問の内容と選択肢を設定する機能を提供する
################################################################################
.controller('WebTestsQuestionController', ['$scope', '$uibModalInstance', '$log', 'WebTestService', 'question', ($scope, $uibModalInstance, $log, WebTestService, question) ->

	wtSrv = WebTestService
	$scope.question  = angular.extend({}, question)

	$scope.initialize = ->
		$scope.env = wtSrv.getServiceEnv()
		if (!angular.isArray($scope.question.choices_attributes))
			$scope.question.choices_attributes = []

  # viewに提供するaction
  # 選択肢に関するaction
	$scope.action = {}
	$scope.action.append = ->
		$log.debug('action call: dialog append')
		$scope.question.choices_attributes.push({})

	$scope.action.delete = (choice) ->
		$log.debug('action call: dialog delete')
		index = $scope.question.choices_attributes.findIndex((item, index, array) ->
			return item == choice
		)
		if (index >= 0)
			$scope.question.choices_attributes[index].destroy = 1

	$scope.action.answerFlagClear = ->
		$log.debug('action call: dialog answerFlagClear')
		angular.forEach($scope.question.choices_attributes, (item, index, array) ->
			item.answer_flag = '0'
		)

	$scope.action.answerFlagChange = (choice) ->
		$log.debug('action call: dialog answerFlagChange')
		angular.forEach($scope.question.choices_attributes, (item, index, array) ->
			if (item != choice)
				item.answer_flag = '0'
		)

	$scope.action.save = ->
		$log.debug('action call: dialog save')
		$scope.question.choices_attributes =
			$scope.question.choices_attributes.filter((item, index, array) ->
				return (item.id || !item.destroy)
			)
		angular.copy($scope.question, question)
		$uibModalInstance.close()

	$scope.action.close = ->
		$log.debug('action call: dialog close')
		$uibModalInstance.close()

	# boot controller
	$scope.initialize()
])
################################################################################
#        Type: Controller
#        Name: WebTestsTestControleer
# Description: テストに関する機能を提供する
################################################################################
.controller('WebTestsTestController', ['$scope', '$log', 'WebTestResource', 'WebTestService', ($scope, $log,  WebTestResource, WebTestService) ->

	wtRsc = WebTestResource
	wtSrv = WebTestService

	# viewに提供するaction
	$scope.action.testQuestionPrev = ->
		$log.debug('action call: testQuestionPrev')
		if ($scope.test.currentIndex > 0)
			$scope.test.currentIndex--
		$scope.test.view = 'question'
		$scope.test.question = $scope.content.questions_attributes[$scope.test.currentIndex]

	$scope.action.testQuestionNext = ->
		$log.debug('action call: testQuestionNext')
		if ($scope.test.currentIndex < $scope.test.maxIndex)
			$scope.test.currentIndex++
		$scope.test.view = 'question'
		$scope.test.question = $scope.content.questions_attributes[$scope.test.currentIndex]

	$scope.action.testQuestionLast = ->
		$log.debug('action call: testQuestionLast')
		$scope.test.view = 'question'
		$scope.test.currentIndex = $scope.test.maxIndex
		$scope.test.question = $scope.content.questions_attributes[$scope.test.maxIndex]

	$scope.action.testConfirm = ->
		$log.debug('action call: testConfirm')
		$scope.test.view = 'confirm'

	$scope.action.testResult = ->
		$log.debug('action call: testResult')
		wtRsc.answer(wtSrv.createAnswer($scope.content)).$promise.then((data) ->
			$log.debug(data)
			$scope.test.result = data
			$scope.test.view = 'result'
			$log.debug($scope)
		)

	$scope.action.answerFlagChange = (choice) ->
		$log.debug('action call: answerFlagChange')
		angular.forEach($scope.test.question.choices_attributes, (item, index, array) ->
			if (item != choice)
				delete item.selected
		)
])

