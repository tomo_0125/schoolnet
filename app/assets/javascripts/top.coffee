# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
.controller 'TopController', ($scope, $http, $localStorage) ->
	$scope.storage = $localStorage
	$scope.storage = $localStorage.$default({
		current_user: {
			application_categories: []
		}
	})
	$scope.application_categories = $scope.storage.current_user.application_categories

	$scope.$on 'update_storage', (event, args) ->
		console.log('TopController update_storage')
		$scope.application_categories = $scope.storage.current_user.application_categories
		console.log($scope.application_categories)


	$scope.application_tap = (appli) ->
		console.log(appli)
		$scope.active_menu = appli.id
		false

	$scope.close_popup = ->
		$scope.active_menu = 0
