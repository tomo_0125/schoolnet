angular.module('MainApp', ['ui.bootstrap', 'ng-token-auth', 'ngStorage', 'ngAnimate', 'ngResource', 'ngFileUpload'])
.config ($authProvider, API_ENDPOINT) ->
	$authProvider.configure({
		apiUrl: API_ENDPOINT.url,
		tokenValidationPath: '/auth/validate_token',
		emailSignInPath: '/auth/sign_in'
	})

.config ($httpProvider) ->
	$httpProvider.interceptors.push('AuthInterceptor')
	#$httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')

.controller 'AppController', ($scope, $localStorage) ->
	$scope.$on 'update_storage', (event, args) ->
		console.log('AppController update_storage')

