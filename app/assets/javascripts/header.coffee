angular.module('MainApp')
.controller('HeaderController', ($scope, $http, $uibModal, $localStorage, Authenticate) ->
	$scope.storage = $localStorage.$default({
		current_user: false
	})
	console.log($scope.storage)

	$scope.login_data = {}
	$scope.login_data.school_id = "school1"
	$scope.login_data.login_id = "student1"
	$scope.login_data.password = "1234abcd"

	$scope.show_login_modal = ->
		console.log("show_login_modal")
		modal = $uibModal.open({
			template: '<div class="row">
									<div class="col-xs-12">
										<input type="text" placeholder="School ID" ng-model="login_data.school_id">
										<input type="text" placeholder="Login ID" ng-model="login_data.login_id">
										<input type="password" placeholder="Password" ng-model="login_data.password">
									</div>
									<div class="col-xs-12">
										<button class="btn btn-default" ng-click="submit_auth()">Sign in</button>
										<button class="btn btn-default" ng-click="close_login_modal()">Close</button>
									</div>
								</div>',
			controller: 'SigninController',
			scope: $scope
    })

		$scope.closeLogin = ->
			modal.hide()

		modal.result.then(-> 
			console.log('close')
		, -> 
			console.log('dismiss')
		)

	$scope.logout = ->
		$uri = 'http://localhost:3000/auth/sign_out'

		$http({
			method : 'DELETE',
			url : $uri
		}).success((data, status, headers, config)->
			$scope.storage.current_user = false
			location.href = "/"
		).error((data, status, headers, config)->
			console.log(status);
		);

	$scope.your_name = ->
		$scope.storage.current_user.user.name

	$scope.is_authenticated = ->
		Authenticate.is_authenticated()

	$scope.validate = ->
		$uri = 'http://localhost:3000/auth/validate_token'

		$http({
			method : 'GET',
			url : $uri
		}).success((data, status, headers, config)->
			console.log(data);
		).error((data, status, headers, config)->
			console.log(status);
		);

	$scope.get_user = ->
		#console.log(angular.fromJson($scope.storage.current_user))
		console.log($scope.storage)

	$scope.get_file_uploads = ->
		$uri = 'http://localhost:3000/api/file_uploads'

		$http({
			method : 'GET',
			url : $uri
		}).success((data, status, headers, config)->
			console.log(data);
			$scope.file_uploads = data
		).error((data, status, headers, config)->
			console.log(status);
		);

)

.controller 'SigninController', ($rootScope, $scope, $uibModalInstance, Authenticate) ->

	$scope.submit_auth = ->
		Authenticate.login($scope, $rootScope)
		$uibModalInstance.close()
	$scope.close_login_modal = ->
		$uibModalInstance.dismiss()

