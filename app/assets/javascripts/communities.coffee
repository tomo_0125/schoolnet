# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
.controller('CommunitiesController', ['$scope', '$http', '$log', ($scope, $http, $log) ->
	uri = 'http://localhost:3000/api/communities'

	$scope.model = {}
	$scope.get_index = ->
		$scope.list()
	$scope.list = ->
		$http.get(uri)
		.success((data, status, headers, config) ->
			$scope.model.list = data
			$scope.view = 'list'
		)
		.error((data, status, headers, config) ->
			$log.error('status: ' + status)
		)

	$scope.show = (content) ->
		if (content)
			$scope.model.show = content
		$scope.view = 'show'
	$scope.new = ->
		$scope.view = 'new'
	$scope.create = ->
		$http.post(uri, $scope.model.new)
		.success((data, status, headers, config) ->
			$log.debug('create success.')
		)
		.error((data, status, headers, config) ->
			$log.debug('create error')
		)
	$scope.edit = ->
		$scope.model.edit = $scope.model.show
		$scope.view = 'edit'
	$scope.update = ->
		$http.patch(uri + '/' + $scope.model.edit.id, $scope.model.edit)
		.success((data, status, headers, config) ->
			$log.debug('update success.')
		)
		.error((data, status, headers, config) ->
			$log.debug('update error.')
		)
	$scope.delete = ->
		$http.delete(uri + '/' + $scope.model.show.id)
		.success((data, status, headers, config) ->
			$log.debug('delete success.')
		)
		.error((data, status, headers, config) ->
			$log.debug('delete error.')
		)
	$scope.add = ->
    console.log("add")
	$scope.list()
])
