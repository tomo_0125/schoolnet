# Place aml the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
.factory('FileUploadService', [ () ->
	return {
		medium_status : {
			new: 'new',
			uploaded: 'uploaded',
			reupload: 'reupload'
		},

		create_content: (assign) ->
			return angular.extend({
				media_attributes: [this.create_medium()]
			}, assign)

		create_medium: (assign) ->
			return angular.extend({
				title: '',
				local_status: this.medium_status.new
			}, assign)

		request_filter: (data) ->

			# optimize media array
			mstatus = this.medium_status
			marray = []
			angular.forEach(data.content.media_attributes, (media, idx) ->
				if (media.local_status == mstatus.new && !media.item instanceof File)
					return

				clone = angular.extend({}, media)
				if (clone.local_status == mstatus.uploaded)
					delete clone.item
				delete clone.local_status
				marray.push(clone)
			)
			data.content.media_attributes = marray

			return data

		response_filter: (data) ->
			data = angular.fromJson(data)
			mstatus = this.medium_status

			filter = (content) ->
				angular.forEach(content.media_attributes, (media, idx) ->
					media.local_status = mstatus.uploaded
				)
			if (angular.isArray(data))
				angular.forEach(data, (content, idx) ->
					filter(content)
				)
			else
				filter(data)

			return data
	}
])
.factory('FileUploadResource', ['$resource', 'FileUploadService', 'UtilityService', ($resource, FileUploadService, UtilityService) ->
	return $resource('http://localhost:3000/api/file_uploads/:id', {
			id: '@id'
		}, {
			query: {
				method: 'GET',
				isArray: true,
				transformResponse: (data, headers) ->
					return FileUploadService.response_filter(data)
			},
			get: {
				isArray: false,
				transformResponse: (data, headers) ->
					return FileUploadService.response_filter(data)
			},
			save: {
				method: 'POST',
				headers: {
          'Content-Type': undefined,
          enctype:'multipart/form-data'
				},
				transformRequest: (data, headersGetter) ->
					data = FileUploadService.request_filter(data)
					return UtilityService.resouce_to_formdata(data, headersGetter)
			},
			update: {
				method: 'PATCH',
				headers: {
					'Content-Type': undefined,
					enctype:'multipart/form-data'
				},
				transformRequest: (data, headersGetter) ->
					data = FileUploadService.request_filter(data)
					return UtilityService.resouce_to_formdata(data, headersGetter)
			}
		})
])
.controller('FileUploadsController', ['$scope',  '$log', 'FileUploadResource', 'UserListResource', 'FileUploadService', 'UtilityService', ($scope, $log, FileUploadResource, UserListResource, FileUploadService, UtilityService) ->

	fu_rsc = FileUploadResource;
	ul_rsc = UserListResource;
	fu_svc = FileUploadService;

	$scope.action = (action, data, options) ->
		switch action
			when 'list_from'
				fu_rsc.query().$promise
				.then((res) ->
					$scope.list = res
					$scope.viewmode = 'list'
				)
			when 'list_to'
				console.log('list_to')
			when 'detail'
				fu_rsc.get({id: data.id}).$promise
				.then((res) ->
					$scope.content = res
					$scope.viewmode = 'detail'
				)
			when 'edit'
				ul_rsc.query().$promise
				.then((res) ->
					$scope.form.users = res
					if (data)
						fu_rsc.get({id:data.id}).$promise
						.then((res) ->
							console.log(res)
							$scope.content = fu_svc.create_content(res)
							angular.element(document).find('.sn-ui-datepicker').trigger('apply_datepicker')
							$scope.viewmode = 'edit'
						)
					else
						$scope.content = fu_svc.create_content()
						angular.element(document).find('.sn-ui-datepicker').trigger('clear_datepicker')
						$scope.viewmode = 'new'
				)
			when 'create'
				$log.debug($scope.content)
				fu_rsc.save({id:$scope.content.id}, {content:$scope.content}).$promise
				.then((res) ->
					$scope.action('list_from')
				)
			when 'update'
				$log.debug($scope.content)
				fu_rsc.update({id:$scope.content.id}, {content:$scope.content}).$promise
				.then((res) ->
					$scope.action('list_from')
				)
			when 'delete'
				fu_rsc.delete({id:data.id}).$promise
				.then((res) ->
					$scope.action('list_from')
				)
			when 'addfile'
				data.push({})
			when 'delfile'
				if (data.local_status == fu_svc.medium_status.uploaded ||
						 data.local_status == fu_svc.medium_status.reupload)
					data.destroy = 1
				else if (data.local_status == fu_svc.medium_status.new)
					$scope.content.media_attributes.splice(options.index, 1)

				cnt = 0
				angular.forEach($scope.content.media_attributes, (medium, idx) ->
					if (!medium.destroy)
						cnt++
				)
				if (cnt == 0)
					$scope.content.media_attributes.push(fu_svc.create_medium())

	# utility
	$scope.util = {
		get_filename: (str) ->
			path = decodeURI(str)
			return UtilityService.get_filename(path)
	}

	$scope.form = {}
	$scope.action('list_from')
])

