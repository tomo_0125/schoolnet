angular.module('MainApp')
 
.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
})

.constant('USER_ROLES', {
  admin: 'admin_role',
  teacher: 'teacher_role',
  student: 'student_role',
  parent: 'parent_role',
})

.constant('API_ENDPOINT', {
  url: 'http://localhost:3000'
});

