// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require angular/angular.min
//= require angular-cookie/angular-cookie.min
//= require angular-bootstrap/ui-bootstrap-tpls.min
//= require angular-resource/angular-resource
//= require danialfarid-angular-file-upload/dist/ng-file-upload-all.js
//= require moment/min/moment-with-locales.min.js
//= require eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js
//= require angular-animate/angular-animate.min
//= require ng-token-auth/dist/ng-token-auth.min
//= require ngstorage/ngStorage.min
//= require main_app
//= require utility_service
//= require utility_directive
//= require authenticate
//= require header
//= require constant
//= require top
//= require movie_uploads
//= require absenses
//= require boards
//= require communities
//= require emergencies
//= require enquetes
//= require facilities
//= require user_lists
//= require file_uploads
//= require messages
//= require ng_words
//= require study_plans
//= require user_settings
//= require web_tests
// require_tree .
