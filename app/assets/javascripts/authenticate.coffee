angular.module('MainApp')

.service 'Authenticate', class Authenticate
	constructor: ($rootScope, $q, $auth, $localStorage) ->
		@q = $q
		@auth = $auth
		@localStorage = $localStorage
		@rootScope = $rootScope

	login: (scope, rootScope)->
		#sync?
		@auth.submitLogin(scope.login_data).then((resp) ->
			console.log("--- response success ---")
			#scope.storage.current_user = angular.toJson(resp)
			scope.storage.current_user = resp
			console.log(resp)
			rootScope.$broadcast('update_storage', [])
		).catch (resp) ->
			console.log(resp)
			return false

	is_authenticated: ->
		@localStorage.current_user != false

	current_user: ->
		@localStorage.current_user

.factory('AuthInterceptor', ($rootScope, $q, AUTH_EVENTS) ->
	return {
		request: (config) ->
			console.log(config.headers["access-token"])
			return config

		#response: (response) ->
		#requestError: (rejection) ->
		responseError: (response) ->
			console.log("AuthInterceptor responseError")
			$rootScope.$broadcast({
				401: AUTH_EVENTS.notAuthenticated,
				403: AUTH_EVENTS.notAuthorized
			}[response.status], response)
			return $q.reject(response)
		}
) 

