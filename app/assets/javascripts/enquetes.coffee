# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
################################################################################
#        Type: Factory
#        Name: EnqueteService
# Description: アンケートに関するビジネスロジックを提供する
#              (Viewに関係のないデータの加工, 設定の定義など)
################################################################################
.factory('EnqueteService', ->
	return {
		getServiceEnv: ->
			return {
				questionTypes:
					selectOne: '1'
					multiSelect: '2'
				
				listTypes:
					from: 'from'
					to: 'to'
			}

		requestFilter: (data, headersGetter) ->
			if (angular.isArray(data.content.questions_attributes))
				data.content.questions_attributes =
					data.content.questions_attributes.filter((item, index, array) ->
						return (item.id || !item.destroy)
					)
			return angular.toJson(data)

		createAnswer: (content) ->
			data =
				answer_history:
					content_id: content.id
					choices_attributes: []

			angular.forEach(content.questions_attributes, (question, qidx) ->
				angular.forEach(question.choices_attributes, (choice, cidx) ->
					if (choice.selected)
						data.answer_history.choices_attributes.push(
							choice_id: choice.id
							free_word: choice.free_word
						)
				)
			)
			return data
	}
)
################################################################################
#        Type: Factory
#        Name: EnqueteResource
# Description: RESTfulによるデータ受け渡しをラップして提供する
################################################################################
.factory('EnqueteResource', ['$resource', 'EnqueteService', ($resource, EnqueteService) ->
	return $resource('http://localhost:3000/api/enquetes/:id', { id: '@id' },
		usersList:
			method: 'GET',
			# TODO: 同一ドメインに対してもプロトコルとドメインが必要な問題
			url: 'http://localhost:3000/api/enquetes/users_list'
			isArray: true

		answer:
			method: 'POST'
			url: 'http://localhost:3000/api/enquetes/answer'
			isArray: false

		save:
			method: 'POST'
			transformRequest: EnqueteService.requestFilter
		
		update:
			method: 'PATCH'
			transformRequest: EnqueteService.requestFilter
	)
])
################################################################################
#        Type: Controller
#        Name: EnqueteController
# Description: アンケートのCURD, リスト表示する機能を提供する
################################################################################
.controller('EnquetesController', ['$scope', '$log', '$q', '$uibModal', 'EnqueteResource', 'EnqueteService', ($scope, $log, $q, $uibModal, EnqueteResource, EnqueteService) ->

	eqtRsc = EnqueteResource
	eqtSrv = EnqueteService

	$scope.initialize = ->
		$scope.env = eqtSrv.getServiceEnv()
		$scope.listMode = $scope.env.listTypes.from
		$scope.action.list()

	$scope.action = {}
	privateAction = {}

	# controller内だけに提供するaction
	privateAction = {}
	privateAction.listFrom = ->
		$log.debug('private action call: listFrom')
		dfd = $q.defer()
		eqtRsc.query().$promise.then((data) ->
			$scope.list = data
			$scope.viewMode = 'list'
			dfd.resolve(data)
		)
		.catch((data) ->
			dfd.reject(data)
		)
		dfd.promise

	privateAction.listTo = ->
		$log.debug('private action call: listTo')
		dfd = $q.defer()
		eqtRsc.usersList().$promise.then((data) ->
			$scope.list = data
			$scope.viewMode = 'list'
			dfd.resolve(data)
		)
		.catch((data) ->
			dfd.reject(data)
		)
		dfd.promise

	privateAction.initDatePicker = ->
		angular.element(document).find('.sn-ui-datepicker').trigger('apply_datepicker')

	# viewに提供するaction
	# アンケートの管理に関するaction
	$scope.action.list = (type) ->
		if (angular.isString(type))
			$scope.listMode = type
		$log.debug("action call: list (listMode: #{$scope.listMode})")
		switch $scope.listMode
			when $scope.env.listTypes.from
				action = privateAction.listFrom
			when $scope.env.listTypes.to
				action = privateAction.listTo
			else
				return
		$scope.list = {}
		$scope.listLoading = true
		action().finally(() ->
			$scope.listLoading = false
		)

	$scope.action.show = (id) ->
		$log.debug('action call: show')
		eqtRsc.get({ id: id }).$promise.then((data) ->
			$scope.content = data
			$scope.viewMode = 'detail'
			$scope.showMode = 'summary'
		)

	$scope.action.detail = (showMode) ->
		$log.debug("action call: detail (#{showMode})")
		$scope.showMode = showMode

	$scope.action.new = ->
		$log.debug('action call: new')
		$scope.content = {}
		$scope.viewMode = 'edit'
		$scope.formMode = 'summary'

	$scope.action.edit = (id) ->
		$log.debug('action call: edit')
		eqtRsc.get({ id: id }).$promise.then((data) ->
			$scope.content = data
			privateAction.initDatePicker()
			$scope.viewMode = 'edit'
			$scope.formMode = 'summary'
		)

	$scope.action.form = (formMode) ->
		$log.debug("action call: form (formMode:#{formMode})")
		$scope.formMode = formMode

	$scope.action.save = ->
		$log.debug('action call: save')
		eqtRsc.save({ id: $scope.content.id }, { content: $scope.content }).$promise.then((res) ->
			$scope.action.list()
		)
	
	$scope.action.update = ->
		$log.debug('action call: update')
		eqtRsc.update({ id: $scope.content.id }, { content: $scope.content }).$promise.then((res) ->
			$scope.action.list()
		)

	$scope.action.delete = (id) ->
		$log.debug('action call: delete')
		eqtRsc.delete({ id: id }).$promise.then((res) ->
			$scope.action.list()
		)

	$scope.action.questionShow = (question) ->
		$log.debug('action call: questionShow')
		$uibModal.open(
			templateUrl: 'question-dialog-show'
			controller: 'EnqueteQuestionController'
			resolve:
				question: ->
					return question
			backdrop: 'static'
			size: 'lg'
		)

	$scope.action.questionEdit = (question) ->
		$log.debug('action call: questionEdit')
		$uibModal.open(
			templateUrl: 'question-dialog-edit'
			controller: 'EnqueteQuestionController'
			resolve:
				question: ->
					return question
			backdrop: 'static'
			size: 'lg'
		)

	$scope.action.questionAppend =  ->
		$log.debug('action call: questionAppend')
		if (!angular.isArray($scope.content.questions_attributes))
			$scope.content.questions_attributes = []
		$scope.content.questions_attributes.push(
			question_type: $scope.env.questionTypes.selectOne
		)

	$scope.action.questionDelete = (question) ->
		$log.debug('action call: questionDelete')
		question.destroy = 1

	# アンケートに関するaction
	$scope.action.enquete = (id) ->
		$log.debug('action call: enquete')
		eqtRsc.get({ id: id }).$promise.then((data) ->
			$scope.content = data
			$scope.enquete =
				currentIndex: 0
				maxIndex: data.questions_attributes.length - 1
				question: data.questions_attributes[0]
				view: 'question'
			$scope.viewMode = 'enquete'
		)

	# boçot controller
	$scope.initialize()
])
################################################################################
#        Type: Controller
#        Name: EnqueteQuestionController
# Description: 質問の内容と選択肢を設定する機能を提供する
################################################################################
.controller('EnqueteQuestionController', ['$scope', '$uibModalInstance', '$log', 'EnqueteService', 'question', ($scope, $uibModalInstance, $log, EnqueteService, question) ->

	eqtSrv = EnqueteService
	$scope.question  = angular.extend({}, question)

	$scope.initialize = ->
		$scope.env = eqtSrv.getServiceEnv()
		if (!angular.isArray($scope.question.choices_attributes))
			$scope.question.choices_attributes = []

  # viewに提供するaction
  # 選択肢に関するaction
	$scope.action = {}
	$scope.action.append = ->
		$log.debug('action call: dialog append')
		$scope.question.choices_attributes.push({})

	$scope.action.delete = (choice) ->
		$log.debug('action call: dialog delete')
		index = $scope.question.choices_attributes.findIndex((item, index, array) ->
			return item == choice
		)
		if (index >= 0)
			$scope.question.choices_attributes[index].destroy = 1

	$scope.action.answerFlagClear = ->
		$log.debug('action call: dialog answerFlagClear')
		angular.forEach($scope.question.choices_attributes, (item, index, array) ->
			item.answer_flag = '0'
		)

	$scope.action.answerFlagChange = (choice) ->
		$log.debug('action call: dialog answerFlagChange')
		angular.forEach($scope.question.choices_attributes, (item, index, array) ->
			if (item != choice)
				item.answer_flag = '0'
		)

	$scope.action.save = ->
		$log.debug('action call: dialog save')
		$scope.question.choices_attributes =
			$scope.question.choices_attributes.filter((item, index, array) ->
				return (item.id || !item.destroy)
			)
		angular.copy($scope.question, question)
		$uibModalInstance.close()

	$scope.action.close = ->
		$log.debug('action call: dialog close')
		$uibModalInstance.close()

	# boot controller
	$scope.initialize()
])
################################################################################
#        Type: Controller
#        Name: EnqueteAnswerControleer
# Description: アンケートの回答に関する機能を提供する
################################################################################
.controller('EnqueteAnswerController', ['$scope', '$log', 'EnqueteResource', 'EnqueteService', ($scope, $log,  EnqueteResource, EnqueteService) ->

	eqtRsc = EnqueteResource
	eqtSrv = EnqueteService

	# viewに提供するaction
	$scope.action.enqueteQuestionPrev = ->
		$log.debug('action call: enqueteQuestionPrev')
		if ($scope.enquete.currentIndex > 0)
			$scope.enquete.currentIndex--
		$scope.enquete.view = 'question'
		$scope.enquete.question = $scope.content.questions_attributes[$scope.enquete.currentIndex]

	$scope.action.enqueteQuestionNext = ->
		$log.debug('action call: enqueteQuestionNext')
		if ($scope.enquete.currentIndex < $scope.enquete.maxIndex)
			$scope.enquete.currentIndex++
		$scope.enquete.view = 'question'
		$scope.enquete.question = $scope.content.questions_attributes[$scope.enquete.currentIndex]

	$scope.action.enqueteQuestionLast = ->
		$log.debug('action call: enqueteQuestionLast')
		$scope.enquete.view = 'question'
		$scope.enquete.currentIndex = $scope.enquete.maxIndex
		$scope.enquete.question = $scope.content.questions_attributes[$scope.enquete.maxIndex]

	$scope.action.enqueteConfirm = ->
		$log.debug('action call: enqueteConfirm')
		$scope.enquete.view = 'confirm'

	$scope.action.enqueteResult = ->
		$log.debug('action call: enqueteResult')
		eqtRsc.answer(eqtSrv.createAnswer($scope.content)).$promise.then((data) ->
			$log.debug(data)
			$scope.enquete.result = data
			$scope.enquete.view = 'result'
			$log.debug($scope)
		)

	$scope.action.answerFlagChange = (choice) ->
		$log.debug('action call: answerFlagChange')
		angular.forEach($scope.enquete.question.choices_attributes, (item, index, array) ->
			if (item != choice)
				delete item.selected
		)
])

