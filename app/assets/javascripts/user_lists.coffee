# Place aml the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('MainApp')
.factory('UserListResource', ['$resource',  ($resource) ->
	return $resource('http://localhost:3000/api/user_list/:id', {
		id: '@id'
	})
])

