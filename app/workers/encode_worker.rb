class EncodeWorker
	include Sidekiq::Worker

	sidekiq_options queue: :encode, unique: :all, retry: false

	def perform(now)
		puts "EncodeWorker perform #{now}"
		Medium.wait.each do |media|
			encode_path = "tmp/encoded_movie_#{media.id}.mp4"

			begin
				puts "############## #{media.item.path}"
				movie = FFMPEG::Movie.new(media.item.path)
				movie.transcode(encode_path)
				media.item = File.open(encode_path)
				media.encode_status = Medium::encode_statuses[:complete]
				File.unlink(encode_path)
				media.save!
			rescue
				media.encode_status = Medium::encode_statuses[:error]
				File.unlink(encode_path)
				media.save!
			end
		end
	end
end
